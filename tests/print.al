# Test 11: NFA to DFA Conversion

Def void recurse(State s, StateSet set) {
	for each State state in s.trans('epsilon') {
		if (!set.contains(state)) {
			set = set & {state};
			recurse(state, set);	
		}	
	}
}

Def StateSet epsClosure(State s) {
	StateSet set = {s};
	for each State state in s.trans('epsilon') {
		if (!set.contains(state)) {
			set = set & {state};
			recurse(state, set);	
		}
	}
	return set;
}

# Recursively computes subset construction
Def void NFAConversion(StateSet t, Set all, Automaton m) {
	for each Char ch in m.alphabet {
		StateSet u;		
		for each State state in t {	# StateSet for loop
			StateSet reachable = state.trans(ch);
			for each State st in reachable {
				u = u & epsClosure(st);		# StateSet union
			}
		}
		if (!u.isEmpty()) {			
			t.trans(ch) = u;
			if (!all.contains(u)) {
				all = all & {u};				# Set union
				NFAConversion(u, all, m);
			}
		}
	}
}

Def Automaton NFAtoDFA(Automaton m) {
	StateSet starting = epsClosure(m.start);
	Set all = {starting};
	NFAConversion(starting, all, m); # recursively computes subset construction	
	
	# Use built in function to create a DFA from the StateSets and transitions found
	Automaton dfa = makeDFAFromStateSets(all, m);
	return dfa;
}

Automaton nfa {
	State A, B, C, D;
	StateSet states = {A, B, C, D};
	State start = A;
	StateSet accept = {B};
	Alphabet alphabet = {'a','b'};
	# Transitions
	
	A.trans('epsilon') = {B}; 
	B.trans('a') = {B, C};
	C.trans('b') = {D};
	D.trans('epsilon') = {B}; 
};

Automaton dfa = NFAtoDFA(nfa);

# Correct input
if (dfa.run("abaab")) {
	print("Success");
} else {
	print("Failure");
}

# Incorrect input
if (!dfa.run("abb")) {
	print("Success");
} else {
	print("Failure");
}
