# Test 7: For Statements
 Automaton myAhoMachine {
 	State begin, s1, s2, s3;
	StateSet states = {begin, s1, s2, s3};
	State start = begin;
	StateSet accept = {s3};
	Alphabet alphabet = {'a','h','o'};
	# Transitions
	begin.trans('a') = {s1}; # begin transitions to s1 on 'a'
	s1.trans('h') = {s2};
	s2.trans('o') = {s3};
 };

 #classic for loop
 for (2) {
 	print("Success");
 }


#for each char - should print success 3 times
for each Char ch in myAhoMachine.alphabet {
	print("Success");
}

#for each char - should print success 4 times
for each State s in myAhoMachine.states {
	print("Success");
}