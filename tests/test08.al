# Test 8: Modifying states & transitions within a for loop, adding destination states to transitions

 Automaton myAhoMachine {
 	State begin, s1, s2, s3;
	StateSet states = {begin, s1, s2, s3};
	State start = begin;
	StateSet accept = {s3};
	Alphabet alphabet = {'a','h','o'};
	# Transitions
	begin.trans('a') = {s1};  # begin transitions to s1 on 'a'
	s1.trans('h') = {s2};
	s2.trans('o') = {s3};
	s1.trans('h') = {s2};
	s2.trans('o') = {s3};
 };

# add a transition on a from every state to s3
for each State s in myAhoMachine.states {
	s.trans('a') = {s3} & s.trans('a'); # overwrites transitions to include s3
}

# Correct input - should still work
if (myAhoMachine.run("aho")) {
	print("Success");
} else {
	print("Failure");
}

# Correct input - new, should accept
if (myAhoMachine.run("aa")) {
	print("Success");
} else {
	print("Failure");
}

# Incorrect input
if (!myAhoMachine.run("bha")) {
	print("Success");
} else {
	print("Failure");
}
