 # Test 2: Automaton from String

 Automaton helloWorld = automaton("hello world");

 # Correct input
 if (helloWorld.run("hello world")) {
 	print("Success");
 } else {
 	print("Failure");
 }

 # Incorrect input
 if (!helloWorld.run("hell")) {
  	print("Success");
 } else {
 	print("Failure");
 }