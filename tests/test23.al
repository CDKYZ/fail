# Test 23: Testing "doublefail" from LRM manual

Automaton atom = automaton("fail");

if (atom.run("fail")) {
	atom = automaton("success");
} else {
	atom = automaton("double") + atom;
}

if (atom.run("success")) {
	print("Success");
} else {
	print("Failure");
}