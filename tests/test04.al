# Test 4: Automaton Union Operator
 
 Automaton test1 = automaton("abcd");
 Automaton test2 = automaton("efg");
 Automaton unionTest = test1 | test2;

 # Correct input
 if (unionTest.run("abcd")) {
 	print("Success");
 } else {
 	print("Failure");
 }

 if (unionTest.run("efg")) {
 	print("Success");
 } else {
 	print("Failure");
 }

 # Incorrect input
 if (!unionTest.run("efgg")) {
   	print("Success");
 } else {
 	print("Failure");
 }
