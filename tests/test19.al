# Test 19 - Testing "Big Example" from the Tutorial

Automaton emailStart {
	State name, at, domainPrefix, valid;
	StateSet states = {name, at, domainPrefix, valid};
	State start = name;
	StateSet accept = {valid};
	Alphabet alphabet = {LETTERS, NUMBERS, '@'};
	# Transitions
	name.trans(LETTERS,NUMBERS) = {name}; # accept any prefix
	name.trans('@') = {at};
	at.trans(LETTERS,NUMBERS) = {valid};
	valid.trans(LETTERS, NUMBERS) = {valid};
};

Automaton schoolEmail = emailStart + automaton(".edu");
Automaton comEmail = emailStart + automaton(".com");
Automaton govEmail = emailStart + automaton(".gov");

if (comEmail.run("automaton@fun.com")) {
	print("Commercial email");
} 
if (schoolEmail.run("project@plt.edu")) {
	print("Educational email");
} 
if (govEmail.run("taxes@IRS.gov")) {
	print("Government email");
} else {
	print("Not an .edu, .gov, or .com email.");
		# print out the transition table and all taken transitions
	printResult(schoolEmail, "taxes@IRS.gov"); 
}
