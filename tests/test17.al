# Test 17: Testing double Automaton declarations

 Automaton myAhoMachine {
 	State begin, s1, s2, s3;
	StateSet states = {begin, s1, s2, s3};
	State start = begin;
	StateSet accept = {s3};
	Alphabet alphabet = {'a','h','o'};
	# Transitions
	begin.trans('a') = {s1}; # begin transitions to s1 on 'a'
	s1.trans('h') = {s2};
	s2.trans('o') = {s3};
 };

 Automaton nfa {
	State A, B, C, D;
	StateSet states = {A, B, C, D};
	State start = A;
	StateSet accept = {B};
	Alphabet alphabet = {'a','b'};
	# Transitions
	
	A.trans('epsilon') = {B}; 
	B.trans('a') = {B, C};
	C.trans('b') = {D};
	D.trans('epsilon') = {B}; 
};

if (!nfa.states.isEmpty()) {
	if (!myAhoMachine.states.isEmpty()) {
		print("Success");
	}
} else {
	print("Failure");
}

if (nfa.run("a")) {
	if (myAhoMachine.run("aho")) {
		print("Success");
	}
} else {
	print("Failure");
}