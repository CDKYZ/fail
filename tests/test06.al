# Test 6: If Statements & boolean operator !
 Automaton test = automaton("test");

 # nested
 if (!test.run("abc")) {
 	print("Success");
 	if (test.run("test")) {
 		print("Success");
 	}
 } else {
 	print("Failure");
 }

 if (test.run("test")) {
 	print("Success");
 }

 # test else
 if (!test.run("test")) {
 	print("Failure");
 } else {
 	print("Success");
 }