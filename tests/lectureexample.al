# Crazy automaton that goes everywhere on every character

Automaton lectureExample {
	State s1, s2, s3;
	StateSet States = {s1, s2, s3};
	State start = s1;
	StateSet accept = {s3};
	Alphabet alphabet = {'a','b'};
	# transitions
	s1.trans('a') = {s1, s2};
	s1.trans('b') = {s1};
	s2.trans('a', 'b')  = {s3};
};


lectureExample.print();

Def void recurse(State s, StateSet set) {
	for each State state in s.trans('epsilon') {
		if (!set.contains(state)) {
			set = set & {state};
			recurse(state, set);	
		}	
	}
}

Def StateSet epsClosure(State s) {
	StateSet set = {s};
	for each State state in s.trans('epsilon') {
		if (!set.contains(state)) {
			set = set & {state};
			recurse(state, set);	
		}
	}
	return set;
}

# Recursively computes subset construction
Def Set NFAConversion(StateSet t, Set all, Automaton m) {
	for each Char ch in m.alphabet {
		StateSet u;		
		for each State state in t {	# StateSet for loop
			StateSet reachable = state.trans(ch);
			for each State st in reachable {
				u = u & epsClosure(st);		# StateSet union
			}
		}
		if (!u.isEmpty()) {			
			t.trans(ch) = u;
			if (!all.contains(u)) {
				all = all & {u};				# Set union
				all = NFAConversion(u, all, m);
			}
		}
	}
	return all;
}

Def Automaton NFAtoDFA(Automaton m) {
	StateSet starting = epsClosure(m.start);
	Set all = {starting};
	all = NFAConversion(starting, all, m); # recursively computes subset construction	
	
	# Use built in function to create a DFA from the StateSets and transitions found
	Automaton dfa = makeDFAFromStateSets(all, m);
	return dfa;
}

Automaton lectureDFA = NFAtoDFA(lectureExample);

lectureDFA.print();