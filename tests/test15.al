# Test 15: Testing built in eclosure
# same example as writing our own

Automaton nfa {
	State A, B, C, D;
	StateSet states = {A, B, C, D};
	State start = A;
	StateSet accept = {B};
	Alphabet alphabet = {'a','b'};
	# Transitions
	A.trans('epsilon') = {B,D}; 
	B.trans('a') = {B, C};
	B.trans('epsilon') = {A};
	C.trans('b') = {D};
	C.trans('epsilon') = {D};
	D.trans('epsilon') = {B}; 
};

StateSet ec = nfa.eclosure(nfa.start); # should return {A,B,D} 
StateSet ec1 = nfa.eclosure(nfa.states.C); # should return {C,D,B,A}

if (ec.contains(nfa.states.A)) { 
	if (ec.contains(nfa.states.B)) {
		print("Success");
	}
} else {
	print("Failure");
}

if (ec1.contains(nfa.states.C)) { 
	if (ec1.contains(nfa.states.B)) {
		if (ec1.contains(nfa.states.D)) {
			if (ec1.contains(nfa.states.A)) { 
				print("Success");
			}	
		}
	}
} else {
	print("Failure");
}


