# Test 13: Set operators & .contains

State a, b, c, d, e;

StateSet w = {c};
StateSet x = {a, b, d}; 
StateSet y = {a, b, c};
StateSet z = {d, e};


Set foo = {x, y, z}; 
Set bar = {w, x};
Set p = foo & bar; 	# Set p now contains {x, y, z, w}
Set q = foo ^ bar; 	# Set q now contains {x}
Set r = foo - bar;	# Set r now contains {y, z}

# p should contain w, x, y, z
if (p.contains(w)) {
	if (p.contains(x)) {
		if (p.contains(y)) {
			if (p.contains(z)) {
				print("Success");
			}
		}
	}
} else {
	print("Failure");
}

# q should contain x should not contain w, y, z
if (q.contains(x)) {
	if (!q.contains(w)) {
		if (!q.contains(y)) {
			if (!q.contains(z)) {
				print("Success");
			}
		}
	}
} else {
	print("Failure");
}


# r should contain y, z, should not contain w, x
if (!r.contains(w)) {
	if (!r.contains(x)) {
		if (r.contains(y)) {
			if (r.contains(z)) {
				print("Success");
			}
		}
	}
} else {
	print("Failure");
}
