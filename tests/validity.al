/* This file holds all example programs that would either compile correclty (valid) 
or would throw a compilation error for one reason or another. Please add to this if you think of edge cases
 or simply edit as you see fit. */ 




//// valid
Automaton al {
	StateSet states = {a, b, c};
	State start = a;
	StateSet accept = {b, c};
	Alphabet alphabet = {'a', 'b', 'c', 'd'};
	a.trans('a') = {b};
	b.trans('a') = {a};
	b.trans('c') = {c};
	c.trans('b') = {a};
};
if (al.run("a")) {
	print("Success");
} else {
	print("Failure");
}

//// invalid
if (al.run("a")) {
	print("Success");
} else {
	print("Failure");
}
/* error, either type incompatibility, calling run on a State, or undeclared, undefined automaton al */


//// valid
StateSet bar = {a, b, c}; 
StateSet foo = bar;

//// valid
StateSet foo = {bar};

//// invalid
StateSet foo = bar;
/* error, type incompatibility (assigning State to StateSet) */ 

//// valid
Def void printAwesome() {
	print("awesome");
}
printAwesome();

//// invalid
printAwesome(); // error, undeclared function being used