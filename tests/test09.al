# Test 9: Email Example & MACROS

Automaton emailStart {
	State name, at, domainPrefix, valid;
	StateSet states = {name, at, domainPrefix, valid};
	State start = name;
	StateSet accept = {valid};
	Alphabet alphabet = {LETTERS, NUMBERS, '@'};
	# Transitions
	name.trans(LETTERS,NUMBERS) = {name}; # accept any prefix
	name.trans('@') = {at};
	at.trans(LETTERS,NUMBERS) = {valid};
	valid.trans(LETTERS, NUMBERS) = {valid};
};
 
 # Union of automata to accept either ".com" or ".edu"
 Automaton emailEnd = automaton(".com") | automaton(".edu");
 Automaton emailDetector = emailStart + emailEnd;


# Correct input
if (emailDetector.run("aaabbbc23468@gmail.com")) {
	print("Success");
} else {
	print("Failure");
}

# Incorrect input
if (!emailDetector.run("23468@gmail.c")) {
	print("Success");
} else {
	print("Failure");
}

# Incorrect input
if (!emailDetector.run("23468@.com")) {
	print("Success");
} else {
	print("Failure");
}
