# Crazy automaton that goes everywhere on every character

Automaton crazyAuto {
	State s1, s2, s3;
	StateSet States = {s1, s2, s3};
	State start = s1;
	StateSet accept = {s3};
	Alphabet alphabet = {'a','b'};
};

for each State astate in crazyAuto.states {
	for each State bstate in crazyAuto.states {
		for each Char ch in crazyAuto.alphabet {
			# add bstate as a trans target on ch
			astate.trans(ch) = astate.trans(ch) & {bstate};
		}
	}
}


crazyAuto.print();

Def void recurse(State s, StateSet set) {
	for each State state in s.trans('epsilon') {
		if (!set.contains(state)) {
			set = set & {state};
			recurse(state, set);	
		}	
	}
}

Def StateSet epsClosure(State s) {
	StateSet set = {s};
	for each State state in s.trans('epsilon') {
		if (!set.contains(state)) {
			set = set & {state};
			recurse(state, set);	
		}
	}
	return set;
}

# Recursively computes subset construction
Def Set NFAConversion(StateSet t, Set all, Automaton m) {
	for each Char ch in m.alphabet {
		StateSet u;		
		for each State state in t {	# StateSet for loop
			StateSet reachable = state.trans(ch);
			for each State st in reachable {
				u = u & epsClosure(st);		# StateSet union
			}
		}
		if (!u.isEmpty()) {			
			t.trans(ch) = u;
			if (!all.contains(u)) {
				all = all & {u};				# Set union
				all = NFAConversion(u, all, m);
			}
		}
	}
	return all;
}

Def Automaton NFAtoDFA(Automaton m) {
	StateSet starting = epsClosure(m.start);
	Set all = {starting};
	all = NFAConversion(starting, all, m); # recursively computes subset construction	
	
	# Use built in function to create a DFA from the StateSets and transitions found
	Automaton dfa = makeDFAFromStateSets(all, m);
	return dfa;
}

Automaton crazyDFA = NFAtoDFA(crazyAuto);

crazyDFA.print();