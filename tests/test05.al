# Test 5: Automaton Concatenation Operator

 Automaton test1 = automaton("abcd");
 Automaton test2 = automaton("efg");
 Automaton concTest = test1 + test2;

 # Correct input
 if (concTest.run("abcdefg")) {
 	print("Success");
 } else {
 	print("Failure");
 }

 # Incorrect input
 if (!concTest.run("efg")) {
  	print("Success");
 } else {
 	print("Failure");
 }

 # Incorrect input
 if (!concTest.run("abcefg")) {
   	print("Success");
 } else {
 	print("Failure");
 }
