# Test 22 - Testing "heyheyheyheyheyheyheyheyheyhey" Ex from the Tutorial

Automaton a = automaton("hey");
Automaton b = automaton("hey");
for(10) {
	a = a + b;
}

if (a.run("heyheyheyheyheyheyheyheyheyheyhey")) {
	print("Success");
} else {
	print("Failure");
}