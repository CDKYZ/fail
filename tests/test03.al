# Test 3: Automaton Star Operator

 Automaton test = automaton("abcd");
 Automaton starTest = test*;

 # Correct input
 if (starTest.run("abcdabcd")) {
 	print("Success");
 } else {
 	print("Failure");
 }

 # Incorrect input
 if (!starTest.run("abcab")) {
  	print("Success");
 } else {
 	print("Failure");
 }