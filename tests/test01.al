# Test 1: Basic Definition
# Should accept 'aho'

 Automaton myAhoMachine {
 	State begin, s1, s2, s3;
	StateSet states = {begin, s1, s2, s3};
	State start = begin;
	StateSet accept = {s3};
	Alphabet alphabet = {'a','h','o'};
	# Transitions
	begin.trans('a') = {s1}; # begin transitions to s1 on 'a'
	s1.trans('h') = {s2};
	s2.trans('o') = {s3};
 };

 # Correct input
 if (myAhoMachine.run("aho")) {
 	print("Success");
 } else {
 	print("Failure");
 }

 # Incorrect input
 if (!myAhoMachine.run("bho")) {
  	print("Success");
 } else {
 	print("Failure");
 }