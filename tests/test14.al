# Test 14: Testing built in access functions, isEmpty

Automaton nfa {
	State A, B, C, D;
	StateSet states = {A, B, C, D};
	State start = A;
	StateSet accept = {B};
	Alphabet alphabet = {'a','b'};
	# Transitions

	A.trans('epsilon') = {B}; 
	B.trans('a') = {B, C};
	C.trans('b') = {D};
	D.trans('epsilon') = {B}; 
};

# test .state and .trans notation
State test1 = nfa.states.A;


StateSet w = test1.trans('epsilon'); # should return {B}
StateSet x = test1.trans('a'); # should return {}
StateSet y = nfa.states.A.trans('epsilon'); # should return {B}
StateSet z = nfa.states.A.trans('a'); #should return {}

# should be equivalent
StateSet wy = w - y;
if (wy.isEmpty()) {
	print("Success");
} else {
	print("Failure");
}

# should be empty
if (x.isEmpty()) {
	print("Success");
} else {
	print("Failure");
}

# should be empty
if (z.isEmpty()) {
	print("Success");
} else {
	print("Failure");
}
