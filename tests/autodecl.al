Automaton al {
        State a, b, c;
	StateSet states = {a, b, c};
	State start = a;
	StateSet accept = {b, c};
	Alphabet alphabet = {'a', 'b', 'c', 'd'};
	a.trans('a') = {b};
	b.trans('a') = {a};
	b.trans('c') = {c};
	c.trans('b') = {a};
};
