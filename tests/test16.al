# Test 16: Testing automaton assignment --> copying

 Automaton myAhoMachine {
 	State begin, s1, s2, s3;
	StateSet states = {begin, s1, s2, s3};
	State start = begin;
	StateSet accept = {s3};
	Alphabet alphabet = {'a','h','o'};
	# Transitions
	begin.trans('a') = {s1}; # begin transitions to s1 on 'a'
	s1.trans('h') = {s2}; 
	s2.trans('o') = {s3};
 };

 Automaton test = myAhoMachine;

# test should have states named testbegin, tests1, tests2, tests3
if (test.accept.contains(test.states.tests3)) {
	print("Success");
} else {
	print("Failure");
}