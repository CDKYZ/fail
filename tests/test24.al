# Test 24 : Testing "crazy auto" from Tutorial

Automaton crazyAuto {
	State s1, s2, s3;
	StateSet States = {s1, s2, s3};
	State start = s1;
	StateSet accept = {s3};
	Alphabet alphabet = {'a','b'};
};

for each State astate in crazyAuto.states {
	for each State bstate in crazyAuto.states {
		for each Char ch in crazyAuto.alphabet {
			# add bstate to the StateSet that astate transitions to on achar
			astate.trans(ch) = astate.trans(ch) & {bstate};
		}
	}
}


if (crazyAuto.run("aa")) {
	print("Success");
} else {
	print("Failure");
}