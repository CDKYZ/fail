# Test 18: Testing StateSet comma separated declarations and empty declaration

State s1, s2, s3, s4;

StateSet a = {s1, s2}, b = {s2, s3}, c = {s4}, d;


if (a.contains(s1)) {
	if (a.contains(s2)) {
		if (!a.contains(s3)) {
			print("Success");
		}
	}
}

if (!b.contains(s1)) {
	if (b.contains(s2)) {
		if (b.contains(s3)) {
			print("Success");
		}
	}
}

if (c.contains(s4)) {
	if (!c.contains(s2)) {
		print("Success");
	}
}

if (!d.contains(s1)) {
	if (d.isEmpty()) {
		print("Success");
	}
}