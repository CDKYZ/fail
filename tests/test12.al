# Test 12: StateSet operators & .contains

State a, b, c, d, e;

StateSet foo = {a, b, d}; 
StateSet bar = {a, b, c};
StateSet p = foo & bar; 	# StateSet p now contains {a, b, c, d}. # (Any repeats are ignored)
StateSet q = foo ^ bar; 	# StateSet q now contains {a, b}
StateSet r = foo - bar;		# StateSet r now contains {d}
StateSet s = foo & bar - (foo ^ bar); # StateSet s now contains {a, b, c, d} 

# p should contain a, b, c, d
if (p.contains(a)) {
	if (p.contains(b)) {
		if (p.contains(c)) {
			if (p.contains(d)) {
				print("Success");
			}
		}
	}
} else {
	print("Failure");
}

# q should contain a, b, should not contain c, d
if (q.contains(a)) {
	if (q.contains(b)) {
		if (!q.contains(c)) {
			if (!q.contains(d)) {
				print("Success");
			}
		}
	}
} else {
	print("Failure");
}


# r should contain d, should not contain a, b, c
if (!r.contains(a)) {
	if (!r.contains(b)) {
		if (!r.contains(c)) {
			if (r.contains(d)) {
				print("Success");
			}
		}
	}
} else {
	print("Failure");
}

# s should contain {a, b, c,d} 
if (s.contains(c)) {
	if (s.contains(d)) {
		if (s.contains(a)) {
			if (s.contains(b)) {
				print("Success");
			}
		}
	}
} else {
	print("Failure");
}