# Test 10: Epsilon Closure (make sure no inf loops) and Function Definition

Def StateSet recurse(State s, StateSet set) {
	for each State state in s.trans('epsilon') {
		if (!set.contains(state)) {
			set = set & {state};
			set = recurse(state, set);
		}	
	}
	return set;
}

Def StateSet epsilonClosure(State state) {
	StateSet set = {state};
	for each State st in state.trans('epsilon') {
		if (!set.contains(st)) {
			set = set & {st};
			set = recurse(st, set);	
		}
	}
	return set;
}

Automaton nfa {
	State A, B, C, D;
	StateSet states = {A, B, C, D};
	State start = A;
	StateSet accept = {B};
	Alphabet alphabet = {'a','b'};
	# Transitions
	A.trans('epsilon') = {B,D}; 
	B.trans('a') = {B, C};
	B.trans('epsilon') = {A};
	C.trans('b') = {D};
	C.trans('epsilon') = {D};
	D.trans('epsilon') = {B}; 
};

StateSet ec = epsilonClosure(nfa.start); # should return {A,B,D} 
StateSet ec1 = epsilonClosure(nfa.states.C); # should return {C,D,B,A}

if (ec.contains(nfa.states.A)) { 
	if (ec.contains(nfa.states.B)) {
		print("Success");
	}
} else {
	print("Failure");
}

if (ec1.contains(nfa.states.C)) { 
	if (ec1.contains(nfa.states.B)) {
		if (ec1.contains(nfa.states.D)) {
			if (ec1.contains(nfa.states.A)) { 
				print("Success");
			}	
		}
	}
} else {
	print("Failure");
}
