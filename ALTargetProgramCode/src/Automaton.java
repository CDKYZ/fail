/**
 * Class Automaton represents a finite automaton.
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Dianna Hohensee
 * @author Kristie Howard
 * Programming Languages and Translators

 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

import java.util.*;

public class Automaton {

	// These variables define the Automaton
	public String name;
	public StateSet states;
	public State start;
	public StateSet accept;
	public Alphabet alphabet;

	// Used in the running of the automaton 
	public StateSet currentStates, nextStates;

	/**
	 * Constructor for creating a basic Automaton. Requires a valid name,
	 * set of states, starting state, set of accepting states, and an alphabet.
	 * Transitions are defined in each state (i.e. every state knows its own 
	 * transitions to other states), and the Automaton knows which states it has
	 * (through parameter states). 
	 * 
	 * @param name the String name of the resulting automaton
	 * @param states the StateSet containing all of the resulting automaton's states
	 * @param start	the start State of the resulting Automaton. start should be in states
	 * @param accept the StateSet containing one or more accepting states of the resulting
	 * 				automaton. All states in accept should be in states.
	 * @param alphabet the Alphabet for this automaton
	 */
	public Automaton(String name, StateSet states, State start, StateSet accept, Alphabet alphabet)
	{
		StateSet myStates = new StateSet();
		StateSet myAccept = new StateSet();
		
		for (State s : states) 
			myStates.addState(s);
		
		for (State s : accept)
			myAccept.addState(s);
		
		this.states = myStates;
		this.start = start;
		this.accept = myAccept;
		this.alphabet = new Alphabet(alphabet);
		this.name = name;
	}

	/**
	 * "Copy" constructor that renames states in the new Automaton
	 * to avoid naming conflicts. 
	 * 
	 * If the new Automaton has the name "alpha", and Automaton old has a state
	 * S1 with a transition on 'b' to S2, the resulting Automaton
	 * will contain a state alphaS1 with a transition on 'b' to alphaS2.
	 * 
	 * POST: This Automaton contains COPIES of the original States in Automaton old,
	 * where the copied States have been renamed as prefix + original state name.
	 * These copied states have replicated transitions to the copied, renamed equivalents of
	 * their original transition targets. 
	 * 
	 * 
	 * @param name String name for the new Automaton
	 * @param old Automaton to copy
	 */
	public Automaton(String name, Automaton old) {
		StateSet myStates = new StateSet();
		StateSet myAccept = new StateSet();
	
		myStates.addStateSetCopy(old.states, name); //add renamed copies
		this.states = myStates;
		this.start = myStates.getStateByName(name + old.start.getName());
		for (State a : old.accept) {
			myAccept.addState(myStates.getStateByName(name + a.getName()));
		}
		this.accept = myAccept;
		this.alphabet = new Alphabet(old.alphabet);
		this.name = name;
	}

	/**
	 * Constructor for making a new automaton by using 
	 * either the union or concatenation operators on the 
	 * given Automatons alpha and beta.
	 * 
	 * - Neither alpha or beta will be altered after this method is called. 
	 * - The resulting Automaton will have renamed states in the following fashion: 
	 * 		- If alpha had a state S1 with a transition on 'b' to S2, the resulting
	 * 			automaton will have a state alphaS1 with a transition on 'b' to
	 * 			alphaS2.
	 * 
	 * @param name the name of the resulting Automaton
	 * @param alpha one of the two Automatons used in the operation
	 * @param beta the other Automaton used in the operation
	 * @param operator one of "concatenation" or "union"
	 * @throws IllegalArgumentException if operator is not "concatenation" or "union"
	 */
	public Automaton(String name, Automaton alpha, Automaton beta, String operator) throws IllegalArgumentException
	{
		this.name = name;
		if (operator.equals("union")) {
			union(alpha, beta);
		} else if (operator.equals("concatenation")) {
			concatenation(alpha, beta);
		} else {
			throw new IllegalArgumentException("Automaton: Invalid operator String was attempted.\n");
		}
	}


	/**
	 * Constructor for making a new automaton by using 
	 * the star operator on the given Automaton alpha.
	 * 
	 * - Alpha will not be altered after this method is called. 
	 * - The resulting Automaton will have renamed states in the following fashion: 
	 * 		- If alpha had a state S1 with a transition on 'b' to S2, the resulting
	 * 			automaton will have a state alphaS1 with a transition on 'b' to
	 * 			alphaS2.
	 * 
	 * @param name the name of the resulting Automaton
	 * @param alpha the Automaton to perform the star operator on
	 * @param operator should only be "star"
	 * 
	 * @throws IllegalArgumentException if operator is not "star"
	 */
	public Automaton(String name, Automaton alpha, String operator) throws IllegalArgumentException 
	{
		this.name = name;
		if (operator.equals("star")) {
			star(alpha);
		} else {
			throw new IllegalArgumentException("Automaton: Invalid operator String was attempted.\n");
		}
	}


	/**
	 * Constructor for the makeAutomatonFor() syntax. Create an automaton to accept
	 * a given string.
	 * 
	 * @param name the name of the resulting Automaton
	 * @param stringToAccept the string that the created Automaton should accept
	 */
	public Automaton(String name, String stringToAccept) {
		this.name = name;
		makeAutomatonForString(stringToAccept);
	}

	/**
	 * 
	 * Static method to create the Automaton for a NFA to DFA conversion. Given the SEt
	 * of StateSets which represent States in the resulting DFA, create States with the correct
	 * transitions for the DFA. Uses the accepting states of the original NFA to determine which
	 * DFA states are accepting.
	 * 
	 * @param name the name of the resulting Automaton
	 * @param allStateSets the Set of StateSets which represent the States in the resulting Automaton
	 * @param NFA the Automaton to convert - used for alphabet and accepting
	 * @return Automaton 
	 */
	static public Automaton makeDFAFromStateSets(String name, ALSet allStateSets, Automaton NFA) {
		
		StateSet startStateSet = NFA.epsilonClosure(NFA.start);
		//make states for each stateset, tagging as you go

		StateSet dfaStates = new StateSet();
		StateSet dfaAccept = new StateSet();
		State dfaStart = null;

		int i = 0;
		for (StateSet stateSet : allStateSets) {
			State newState = new State("s"+i);
			stateSet.tag = newState; //link with StateSet by tagging
			dfaStates.addState(newState); //add to DFA state set
			if (stateSet.equals(startStateSet))  
				dfaStart = newState; //starting state
			if (stateSet.isAccepting(NFA.accept)) 
				dfaAccept.addState(newState); //add to accepting states
			i++;
		}

		//make sure start state was found
		assert(dfaStart != null);

		//Add transitions
		for (StateSet s : allStateSets) {
			//find matching dfaState
			State dfaState = s.tag;
			//for each transition character, create transition to respective State for target StateSet
			if (s.transitions != null) {
				for (String transChar : s.transitions.keySet()) {
					//find State representing the StateSet 

					StateSet target = s.transitions.get(transChar); //will only be one (bc DFA)
					//second time you access a stateset, it gives you a random one, so get correct one iwth tag
					target = allStateSets.returnMatching(target);
					dfaState.addTransitionTo(target.tag, transChar);
				}
			}
		}
		return new Automaton(name, dfaStates, dfaStart, dfaAccept, NFA.alphabet);
	}



	/**
	 * Helper method for the union constructor.
	 * Uses the union operator on the two given Automatons 
	 * alpha and beta to initialize this automaton's variables.
	 * 
	 * @param alpha first Automaton of the union
	 * @param beta second Automaton of the concatenation
	 * @throws IllegalArgumentException 
	 */
	private void union(Automaton alpha, Automaton beta) throws IllegalArgumentException
	{
		states = new StateSet(alpha.states, beta.states, alpha.name, beta.name, "union");
		alphabet = new Alphabet(alpha.alphabet, beta.alphabet);

		// Make a new start state with epsilon transitions 
		// to the two start states in alpha and beta
		start = new State(name + "Start");
		start.addTransitionStateNoOverwrite(states.getStateByName(alpha.name + alpha.start.getName()), "epsilon");
		start.addTransitionStateNoOverwrite(states.getStateByName(beta.name + beta.start.getName()), "epsilon");		
		states.addState(start);

		// Make a new single accept state, to which all the accept states of 
		// alpha and beta transition on epsilon
		State end = new State(name + "End");
		accept = new StateSet(end);
		states.addState(end);

		for (State state : alpha.accept) {
			states.getStateByName(alpha.name + state.getName()).addTransitionStateNoOverwrite(end, "epsilon");
		}
		for (State state : beta.accept) {
			states.getStateByName(beta.name + state.getName()).addTransitionStateNoOverwrite(end, "epsilon");
		}
	}


	/**
	 * Helper method for the concatenation constructor.
	 * Uses the concatenation operator on the two given Automatons 
	 * alpha and beta to initialize this automaton's variables.
	 * 
	 * @param alpha first Automaton of the concatenation
	 * @param beta second Automaton of the concatenation
	 */
	private void concatenation(Automaton alpha, Automaton beta) throws IllegalArgumentException 
	{
		states = new StateSet(alpha.states, beta.states, alpha.name, beta.name, "union");
		alphabet = new Alphabet(alpha.alphabet, beta.alphabet);

		// Create a new start state with an epsilon transition
		// to our version of alpha's start state
		start = new State(name + "Start");
		states.addState(start);
		start.addTransitionTo(states.getStateByName(alpha.name + alpha.start.getName()), "epsilon");

		// Create epsilon transitions from our version of 
		// alpha's accept states to beta's start state	
		for (State state : alpha.accept) {
			states.getStateByName(alpha.name + state.getName())
			.addTransitionStateNoOverwrite(states.getStateByName(beta.name + beta.start.getName()), "epsilon");
		}

		// Create a new accept state, and transitions from our version
		// of beta's accept states to the new single accept state
		State end = new State(name + "End");
		states.addState(end);
		accept = new StateSet(end);

		for (State state : beta.accept) {
			states.getStateByName(beta.name + state.getName()).addTransitionStateNoOverwrite(end, "epsilon");
		}
	}

	/**
	 * Helper method for the star constructor.
	 * Uses the star operator on the given Automaton alpha
	 * to initialize this automaton's variables
	 * 
	 * @param alpha the automaton to star
	 */
	private void star(Automaton alpha) throws IllegalArgumentException 
	{
		states = new StateSet(alpha.states, alpha.name);
		alphabet = new Alphabet(alpha.alphabet);

		//  Create a new accept state 

		State end = new State(name + "End");
		accept = new StateSet(end);

		/*
		 * Add epsilon transitions from our versions of alpha's 
		 * accept states to our version of alpha's start state
		 * and our accept state  
		 */

		// Iterate through alpha's accept states 
		for (State acceptState : alpha.accept) {
			// Add e-trans from our version of alpha's accept 
			// state to our version of alpha's start state
			states.getStateByName(alpha.name + acceptState.getName())
			.addTransitionStateNoOverwrite(states.getStateByName(alpha.name + alpha.start.getName()), "epsilon");
			// Add e-trans from our version of alpha's accept 
			// state to our accept state
			states.getStateByName(alpha.name + acceptState.getName())
			.addTransitionStateNoOverwrite(end, "epsilon");
		}

		/* Create a new start state with an epsilon transition 
		 * to our version of alpha's start state and our accept state
		 */
		start = new State(name + "Start");
		// Add e-trans to our version of alpha's start state
		start.addTransitionStateNoOverwrite(states.getStateByName(alpha.name + alpha.start.getName()), "epsilon");
		// Add e-trans to our accept state
		start.addTransitionStateNoOverwrite(end, "epsilon");
		states.addState(start);
		states.addState(end);
	}



	/**
	 * Assists the Automaton(String) constructor. In a loop, on each char of the 
	 * input string, it creates a target state, adds it to this.states. 
	 * Creates a transition from the previous to the target state on the input char.
	 * Adds each one of these characters to the alphabet.
	 * At the end, add the last state to accepting.
	 * 
	 * @param stringToAccept
	 */
	private void makeAutomatonForString(String stringToAccept) {

		State start = new State("s0");
		this.start = start;	
		this.states = new StateSet(start); 
		int length = stringToAccept.length();

		//create a target state, add it to this.states
		//create a transition from s(i) to s(i+1) on c
		//add c to alphabet
		for (int i = 0; i < length; i++) {
			String c = stringToAccept.substring(i,i+1);
			if (i == 0)
				this.alphabet = new Alphabet(c);
			else
				this.alphabet.addChar(c);
			State target = new State("s"+(i+1));
			this.states.addState(target);

			State prev = this.states.getStateByName("s"+i);
			if (prev != null) 
				prev.addTransitionTo(target, c);

			//add last state to accept
			if (i == length - 1)
				this.accept = new StateSet(target); 
		}	

	}


	/**
	 * Function that prints a transition table, and will wrap around lines
	 * if the set of states it transitions to extends beyond nextStatesWidth,
	 * the size of the third column.
	 * 
	 * Prints out transitions in three columns:
	 * - state s
	 * - input symbol c
	 * - set of states with transitions from state s on c
	 */
	private void printTransitionTable(){

		int stateWidth = 30; //default vals
		int inputWidth = 8;
		int nextStatesWidth = 50;
		boolean truncated = false;

		String border = createBorderString(stateWidth, inputWidth, nextStatesWidth);
		String leftAlign = "|%-"+ stateWidth +"s | %-"+ inputWidth +"s | %-"+ nextStatesWidth +"s |%n";
		System.out.format(border);
		System.out.format(leftAlign, "   State name", "Input", "Next state(s)");
		System.out.format(border);


		for (State s : this.states) {
			String statename = s.getName();
			if (this.accept.hasState(statename)) //if state is accept, print *
				statename = " * " + statename;
			else if (this.start.getName().equals(s.getName())) //if state is start, print -->
				statename = "-->" + statename;
			else
				statename = "   " + statename;

			if (statename.length() > stateWidth)  {//truncate if too long
				statename = statename.substring(0, stateWidth);
				truncated = true;
			}

			this.alphabet.addChar("epsilon");
			for (String c : this.alphabet) {
				StateSet targets = s.getTransitionStates(c);

				if (!targets.isEmpty()) { //transitions exist!
					String destinationStates = targets.toString();
					int dslength = destinationStates.length();
					if (dslength > nextStatesWidth) { //wrap around code		
						int lines = (int) Math.ceil((double)dslength/nextStatesWidth);

						for (int i = 1; i <= lines; i++) {
							int br = destinationStates.lastIndexOf(" ", nextStatesWidth*i);
							if (i==1) {
								System.out.format(leftAlign, statename, c, destinationStates.substring(0,br));
								destinationStates = destinationStates.substring(br); //cut off what you printed out
							} else if (i == lines) {
								System.out.format(leftAlign, " ", " ", destinationStates); //only has end
							} else {
								System.out.format(leftAlign, " ", " ", destinationStates.substring(0,br));
								destinationStates = destinationStates.substring(br); //cut off what you printed out;
							}
						}
					} else {
						System.out.format(leftAlign, statename, c, destinationStates);
					}	
				}	
			}
		}
		this.alphabet.removeChar("epsilon");
		System.out.format(border);
		System.out.println("NOTE:' * ' designates accepting state, '-->' designates start state");
		if(truncated)
			System.out.println("WARNING: Certain state names may have been truncated. Full names can be found at the top, and states will be listed alphabetically.");
	}

	/**
	 * Helper method to create the string for the top and bottom of the transition table.
	 * 
	 * @param stateWidth int specifying the width of first column
	 * @param inputWidth int specifying the width of second column
	 * @param nextStatesWidth int specifying the width of third column
	 * 
	 * @return format string
	 */
	private String createBorderString(int stateWidth, int inputWidth, int nextStatesWidth) {
		String border = "+";
		for (int i = 0; i < stateWidth+1; i++) 
			border = border + "-";
		border = border + "+";
		for (int i = 0; i < inputWidth+2; i++) 
			border = border + "-";
		border = border + "+";
		for (int i = 0; i < nextStatesWidth+2; i++) 
			border = border + "-";
		border = border + "+%n";
		return border;
	}


	/**
	 * Prints this automaton for easy viewing with the name, alphabet, states, start
	 * accepting states, and a transition table.
	 * 
	 */
	public void printAutomaton() {
		System.out.println();
		System.out.println("Automaton name: "+ this.name);
		System.out.println("Alphabet: "+ this.alphabet.toString());
		System.out.println("States: " + this.states.toString());
		System.out.println("Starting state: " + this.start.getName());
		System.out.println("Accepting states: " + this.accept.toString());
		System.out.println("Transition Table: ");
		this.printTransitionTable();
		System.out.println();
	}


	/**
	 * Runs this automaton on a given input, returning a boolean specifying whether the input
	 * was accepted (true) or rejected (false). 
	 * 
	 * An automaton will reject the input and return false:
	 * - If any input character is not in the alphabet
	 * - If there are no transitions specified on that letter from any of the current states of the 
	 * 		computation
	 * - If the input stream is exhausted and none of the current states are accepting states
	 * 
	 * Otherwise, the input is accepted and true is returned.
	 * 
	 * @param input String to run the Automaton on
	 */
	public boolean run(String input) {		
		/*	On each char in the string
		input, update currentStates and nextStates via searching for the transitions
		on that input char from all current states. */

		currentStates = this.epsilonClosure(start);
		nextStates = new StateSet();

		//remember to clean out nextStates every time you make nextStates current states
		int length = input.length();

		for (int i = 0; i < length; i++) {
			String c = input.substring(i,i+1);

			if (!this.alphabet.contains(c)) {
				return false; 
			}

			//check transitions and e transitions

			for (State curr : currentStates) 
				updateNextStates(curr, c);

			if (nextStates.isEmpty()) {
				return false;
			}
			//end of computation, no transitions

			currentStates.clearEntries();
			currentStates.addStateSet(nextStates);
			nextStates.clearEntries();

		}

		//reached end of input: check currentStates for accepting
		for (State finalState : currentStates) {
			if (accept.hasState(finalState.getName()))
				return true; //one of the final states is an accepting state
		}

		return false; 
	}

	/**
	 * Prints the automaton out, and then runs this automaton on a given input. Prints the steps
	 * taken on each input character, and explains why an Automaton rejects an input if it does so.
	 * 
	 * An automaton will reject the input:
	 * - If any input character is not in the alphabet
	 * - If there are no transitions specified on that letter from any of the current states of the 
	 * 		computation
	 * - If the input stream is exhausted and none of the current states are accepting states
	 * 
	 * Otherwise, the input is accepted.
	 * 
	 * @param input String to run the Automaton on
	 */
	public void printResult(String input) {
		this.printAutomaton();

		System.out.println("\nRunning Automaton " + this.name + " on input " + input + "\n");

		currentStates = this.epsilonClosure(start);
		nextStates = new StateSet();

		System.out.println("Starting in state: " + start.getName());

		//remember to clean out nextStates every time you make nextStates currentStates
		int length = input.length();

		for (int i = 0; i < length; i++) {
			System.out.println();

			String c = input.substring(i,i+1);
			System.out.println("Input char:" + c );


			if (!this.alphabet.contains(c)) {
				System.out.println("\nThe input character " + c + " is not in the alphabet.");
				System.out.println("Automaton " + this.name + " REJECTS the input " + input + ".\n");
				return; 
			}


			//check transitions and e transitions
			for (State curr : currentStates) 
				updateNextStates(curr, c);

			System.out.println("Current states after input " + c + ": "+ nextStates.toString());

			if (nextStates.isEmpty()) {
				System.out.println("\nThere are no outgoing transitions on input character " +
						c + " from any of the current states.");
				System.out.println("Automaton " + this.name + " REJECTS the input " + input + ".\n");
				return;
			}
			//end of computation, no transitions

			currentStates.clearEntries();
			currentStates.addStateSet(nextStates);
			nextStates.clearEntries();

		}

		System.out.println("\nThe input is finished.");
		//reached end of input: check currentStates for accepting
		boolean accepted = false;
		StateSet acceptedFinals = new StateSet();

		for (State finalState : currentStates) {
			if (accept.hasState(finalState.getName())) {
				acceptedFinals.addState(finalState);
				accepted = true; //one of the final states is an accepting state
			}
		}

		if (accepted) {
			System.out.println("The automaton is in the following accepted states: " + acceptedFinals.toString());
			System.out.println("Automaton " + this.name + " ACCEPTS the input " + input + ".\n");
		} else {
			System.out.println("None of the final states were accepting states. ");
			System.out.println("Automaton " + this.name + " REJECTS the input " + input + ".\n");
		}
		System.out.println();

	}


	/**
	 * Helper function. Look at all of current's transitions on character c.
	 * Add any target states to nextStates. Also look for epsilon transitions.
	 * 
	 * @param current the current state we are looking for transitions from on c
	 * @param c
	 */
	private void updateNextStates(State current, String c) {
		StateSet targets = current.getTransitionStates(c);
		if (!targets.isEmpty()) {
			for (State t : targets) {
				nextStates.addStateSet(epsilonClosure(t));
			}
		}
		//check epsilon transitions from this current state,
		//add any possible states we could reach to nextStates
	}


	/**
	 * Function to compute the epsilonClosure
	 * of a state. Returns a StateSet with one or 
	 * more elements consisting of those states reachable by 
	 * epsilon transitions only. Initialized to contain current.
	 * 
	 * @param current state to check epsilon transitions from
	 * @return StateSet with all of the reachable states by only 
	 * epsilon transitions. Will always contain State current.
	 * 
	 */
	public StateSet epsilonClosure(State current) {
		StateSet reachable = new StateSet(current);
		Stack<State> temp = new Stack<State>();
		temp.push(current);
		StateSet visited = new StateSet(current);

		while(!temp.empty()) {
			//pop top element off
			State t = temp.pop();
			StateSet etrans = t.getTransitionStates("epsilon");
			if (!etrans.isEmpty()) {//there are epsilon transitions to other states
				for (State u : etrans) {
					reachable.addState(u);
					if (!visited.contains(u)) {
						visited.addState(u);
						temp.push(u);
					}
				}
			}
		}
		return reachable;
	}


	/**
	 * Helper method for easily generating the Automaton that is the
	 * result of a star operation.
	 * 
	 * @param newName new name for created Automaton
	 * @param alpha	Automaton to star
	 * @return alpha*
	 */
	static public Automaton star(String newName, Automaton alpha) {
		return new Automaton(newName, alpha, "star");
	}





	/**
	 * Helper method for easily generating the Automaton that is the
	 * result of a union operation on Automaton alpha and Automaton beta.
	 * 
	 * @param newName new name for created Automaton
	 * @param alpha	Automaton 1
	 * @param beta Automaton 2
	 * @return alpha | beta
	 */
	static public Automaton union(String newName, Automaton alpha, Automaton beta) {
		return new Automaton(newName, alpha, beta, "union");
	}


	/**
	 * Helper method for easily generating the Automaton that is the
	 * result of a union operation on Automaton alpha and Automaton beta.
	 * 
	 * @param newName new name for created Automaton
	 * @param alpha	Automaton 1
	 * @param beta Automaton 2
	 * @return alpha + beta
	 */
	static public Automaton concatenation(String newName, Automaton alpha, Automaton beta)  {
		return new Automaton(newName, alpha, beta, "concatenation");
	}

	/**
	 * Helper method to generate the Automaton that is the result of a call to the
	 * makeAutomatonFor(String wordToAccept) function in the source language.
	 * 
	 * @param newName variable name for the created Automaton
	 * @param wordToAccept string the Automaton is supposed to accept
	 * @return makeAutomatonFor(String wordToAccept)
	 */
	static public Automaton makeAutomatonFor(String newName, String wordToAccept) {
		return new Automaton(newName, wordToAccept);
	}
}