/**
 * Boilerplate defines a few helper functions used in codegen.
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Yujin Ariza
 * Programming Languages and Translators
 *
 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

public class Boilerplate {

	public static Automaton copy(String id, Automaton a) {
		return new Automaton(id, a);
	}

	public static ALSet copy(String id, ALSet a) {
		return a;
	}

	public static Alphabet copy(String id, Alphabet a) {
		return a;
	}

	public static StateSet copy(String id, StateSet a) {
		return a;
	}

	public static State copy(String id, State a) {
		return a;
	}

	public static ALSet union(ALSet a, ALSet b) {
		return ALSet.union(a,b);
	}

	public static StateSet union(StateSet a, StateSet b) {
		return StateSet.union(a,b);
	}

	public static ALSet difference(ALSet a, ALSet b) {
		return ALSet.difference(a,b);
	}

	public static StateSet difference(StateSet a, StateSet b) {
		return StateSet.difference(a,b);
	}

	public static ALSet intersection(ALSet a, ALSet b) {
		return ALSet.intersection(a,b);
	}

	public static StateSet intersection(StateSet a, StateSet b) {
		return StateSet.intersection(a,b);
	}

	public static ALSet set(StateSet... sets) {
		return new ALSet(sets);
	}

	public static StateSet set(State... states) {
		return new StateSet(states);
	}

}