/**
 * Class ALSet is a Set of StateSets in our source language.
 * ALSets contain StateSet objects.
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Kristie Howard
 * Programming Languages and Translators

 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

import java.util.*;


public class ALSet implements Set<StateSet>{
	private List<StateSet> set; //manually manage no duplicates
	
	
	/**
	 * Constructor for an empty ALSet with no elements
	 */
	public ALSet() {
		set = new ArrayList<StateSet>();
		
	}
	
	/**
	 * Constructor for ALSets that takes a variable number of StateSets
	 * to keep reference to
	 * 
	 * @param statesets a variable number of StateSets to belong in this ALSet
	 */
	public ALSet(StateSet...statesets) {
		set = new ArrayList<StateSet>();
		for (StateSet s: statesets) {
			if (!this.set.contains(s)) {
				set.add(s);
			}
		}
		
	}
	
	
	/**
	 * Static method to compute the union of two ALSets.
	 * Adds every element of the first set, and any unique
	 * elements of the second set. 
	 * 
	 * Source: Set a = b & c;
	 * 
	 * @param first ALSet to union
	 * @param second ALSet to union
	 * @return ALSet representing the union of first and second
	 */
	public static ALSet union(ALSet first, ALSet second) {
		ALSet result = new ALSet();
		for (StateSet s: first.set) {
			if (!s.isEmpty())
				result.add(s);
		}
		
		for (StateSet s : second.set) {
			if (!result.contains(s) && !s.isEmpty()) {
				result.add(s);
			}
		}
		return result;
	}
	
	
	/**
	 * Static method to compute the difference of two ALSets.
	 * Adds every element of the first set, and removes any
	 * elemtns of the second set that are in the first set.
	 * 
	 * Source: Set a = b - c;
	 * 
	 * @param first ALSet
	 * @param second ALSet
	 * @return ALSet representing the set difference of first and second
	 */
	public static ALSet difference(ALSet first, ALSet second) {
		ALSet result = new ALSet();
		for (StateSet s: first.set) {
			// if s is not empty and second does not contain this set, add to result
			if (!s.isEmpty() & !second.contains(s))
				result.add(s);
		}
		
		return result;
	}
	
	
	/**
	 * Static method to compute the intersection of two ALSets.
	 * Keeps elements that are in both ALSets.
	 * 
	 * Source: Set a = b ^ c;
	 * 
	 * @param first ALSet
	 * @param second ALSet
	 * @return ALSet representing the set intersection of first and second
	 */
	public static ALSet intersection(ALSet first, ALSet second) {
		ALSet result = new ALSet();
		for (StateSet s: first.set) {
			// if s is not empty and second DOES contain this set, add to result
			if (!s.isEmpty() & second.contains(s))
				result.add(s);
		}
		
		return result;
	}
	
	
	/**
	 * Helper method for the NFA to DFA conversion backend. Returns
	 * the StateSet, if it exists, in this ALSet that "matches" the
	 * StateSet toMatch. A match is when every element is the same.
	 * If there is no match, it returns the original StateSet.
	 * 
	 * @param toMatch StateSet to search for in this ALSet
	 * @return matching StateSet from this ALSet, or toMatch if none exists
	 */
	public StateSet returnMatching(StateSet toMatch) {
		for (StateSet s: this.set) {
			if (s.equals(toMatch)) {
				return s;
			}
		}
		return toMatch;
		
	}
	
	@Override
	public int size() {
		return set.size();
	}

	@Override
	public boolean isEmpty() {
		return set.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		if (o == null)
			return false;
		for (StateSet element : set) {
			if (element.equals((StateSet)o)) //check if same object
				return true;
		}
		return false;
	}

	@Override
	public Iterator<StateSet> iterator() {
		return set.iterator();
	}

	@Override
	public Object[] toArray() {
		return set.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return set.toArray(a);
	}

	@Override
	public boolean add(StateSet e) {
		if (!this.contains(e)) {
			set.add(e);
			return true;
		}
	
		return false;
	}

	@Override
	public boolean remove(Object o) {
		return set.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return set.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends StateSet> c) {
		return set.addAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return set.retainAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
	
		return set.removeAll(c);
	}

	@Override
	public void clear() {
		set.clear();
	}
	
	@Override
	public String toString() {
		return set.toString();
	}



}
