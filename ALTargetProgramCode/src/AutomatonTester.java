/**
 * Class AutomatonTester is used for testing the Java code
 * of the programs that we wrote in the Tutorial.
 * It is not included in
 * boilerplate.jar (unnecessary for source programs to run)
 * 
 * Part of AL (Automaton Language), 
 * 
 * @author Kristie Howard
 * Programming Languages and Translators

 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

public class AutomatonTester {

	/**
	 * Creates and tests the machine in the "Getting Started" example
	 * from the Language Tutorial. 
	 * 
	 * Automaton myAhoMachine {
	 * 		State begin, s1, s2, s3;
	 * 		Set states = {begin, s1, s2, s3};
	 * 		State start = begin;
	 * 		Set accept = {s3};
	 * 		Alphabet alphabet = {'a','h','o'};
	 * 		# Transitions
	 * 		begin.trans('a') = {s1}; 	# begin transitions to s1 on 'a'
	 * 		s1.trans('h') = {s2};		# 'h'
	 * 		s2.trans('o') = {s3}; 		# 'o'
	 * 	 };
	 * 
	 * 	myAhoMachine.run("aho");
	 * 
	 * @return the final machine to use in other testing functions
	 */
	public Automaton myAhoMachine() {
		State begin = new State("begin");
		State s1 = new State("s1");
		State s2 = new State("s2");
		State s3 = new State("s3");

		StateSet accept = new StateSet(s3);
		StateSet states = new StateSet(begin, s1, s2, s3);
		State start = begin;
		Alphabet alp = new Alphabet("a", "h", "o");

		Automaton myAhoMachine = new Automaton("myAhoMachine", states, start, accept, alp);
		
		begin.addTransitionTo(s1, "a");
		s1.addTransitionTo(s2, "h");
		s2.addTransitionTo(s3, "o");

		//	myAhoMachine.printResult("aho");
		assert(myAhoMachine.run("aho")); //true
		assert(!myAhoMachine.run("ahoo")); //false
		assert(!myAhoMachine.run("bho")); //false

		return myAhoMachine;
	}

	//check for begin.trans(‘a’,’h’) = begin.trans(‘a’,‘h’) & {s2};
	//begin.addTransitionsTo(StateSet.union(begin.getTransitions(“a”), new StateSet(s2))), “a”)
	public Automaton testUnioningTransitions() {
		State begin = new State("begin");
		State s1 = new State("s1");
		State s2 = new State("s2");
		State s3 = new State("s3");

		StateSet accept = new StateSet(s3);
		StateSet states = new StateSet(begin, s1, s2, s3);
		State start = begin;
		Alphabet alp = new Alphabet("a", "h", "o");

		Automaton myAhoMachine = new Automaton("myAhoMachine", states, start, accept, alp);
		
		begin.addTransitionTo(s1, "a");
		s1.addTransitionTo(s2, "h");
		s2.addTransitionTo(s3, "o");

		begin.addTransitionsTo(StateSet.union(begin.getTransitionStates("a", "h"), new StateSet(s2) ),"a");
		begin.addTransitionsTo(StateSet.union(begin.getTransitionStates("a", "h"), new StateSet(s2) ),"h");
		begin.removeTransition("h"); //undoes last move
		myAhoMachine.printResult("aho");
		
		assert(myAhoMachine.run("aho")); //true
		assert(!myAhoMachine.run("ahoo")); //false
		assert(!myAhoMachine.run("bho")); //false

		return myAhoMachine;
	}
	
	
	
	
	
	/**
	 * Automaton helloWorld = automaton("hello world");
	 * 
	 * @return
	 */
	public Automaton helloWorld() {

		Automaton helloWorld = new Automaton("helloWorld", "hello world");

		//	helloWorld.printResult("hello world");

		assert(helloWorld.run("hello world")); //true
		assert(!helloWorld.run("helloworld")); //false

		return helloWorld;
	}
	
	/**
	 * StateSet w = {c};
	 * StateSet x = {a, b, d}; 
	 * StateSet y = {a, b, c};
	 * StateSet z = {d, e};
	 * 
	 * Set foo = {x, y, z};
	 * Set bar = {w, x};
	 * Set p = foo & bar; 			# Set p now contains {x, y, z, w}
	 * Set q = foo ^ bar; 			# Set q now contains {x}
	 * Set r = foo - bar;			# Set r now contains {y, z}
	 */
	public void testSets() {
		State a = new State("a");
		State b = new State("b");
		State c = new State("c");
		State d = new State("d");
		State e = new State("e");
		
		StateSet w = new StateSet(c);
		StateSet x = new StateSet(a, b, d);
		StateSet y = new StateSet(a, b, c);
		StateSet z = new StateSet(d,e);
		
		ALSet foo1 = new ALSet(x, y, z);
		ALSet bar2 = new ALSet(w, x);
		ALSet p1 = ALSet.union(foo1, bar2);
		ALSet q1 = ALSet.intersection(foo1, bar2);
		ALSet r1 = ALSet.difference(foo1, bar2);
		
		System.out.println("p1: " + p1.toString());
		System.out.println("q1: " + q1.toString());
		System.out.println("r1: " + r1.toString());
		
		System.out.println(p1.contains(x));
		System.out.println(p1.contains(y));		
		System.out.println(p1.contains(z));
		System.out.println(p1.contains(w));
		
		System.out.println(q1.contains(x));
		
		System.out.println(r1.contains(y));		
		System.out.println(r1.contains(z));
		

	}

	/**
	 * StateSet foo = {a,b, d}; 
	 * StateSet bar = {a, b, c};
	 * 
	 * StateSet p = foo & bar; 		# StateSet p now contains {a, b, c, d}
	 * StateSet q = foo ^ bar; 		# StateSet q now contains {a, b}
	 * StateSet r = foo - bar;		# StateSet r now contains {d}
	 * StateSet s = foo & bar - (foo ^ bar); # StateSet s now contains {a, b, c, d}
	 */
	public void testStateSets() {
		
		State a = new State("a");
		State b = new State("b");
		State c = new State("c");
		State d = new State("d");
		State e = new State("e");
		
		StateSet foo = new StateSet(a, b, d);
		StateSet bar = new StateSet(a,b,c);
		
		StateSet p = StateSet.union(foo, bar);
		StateSet q = StateSet.intersection(foo, bar);
		StateSet r = StateSet.difference(foo, bar);
		// StateSet s = foo & bar - (foo ^ bar); # StateSet s now contains {a, b, c, d}
		
		StateSet s = StateSet.union(foo,StateSet.difference(bar, StateSet.intersection(foo, bar)));
		
		System.out.println("p: " + p.toString());
		System.out.println("q: " + q.toString());
		System.out.println("r: " + r.toString());
		System.out.println("s: " + s.toString());
		
		//all working

	}

	/**
	 * Tests the star operator..
	 * 
	 * Automaton e = a*;
	 * 
	 * @param alpha 
	 * @param stringToAccept a string that alpha should accept
	 */
	public Automaton testStar(Automaton alpha, String stringToAccept) {
		Automaton star = new Automaton(alpha.name + "Star", alpha, "star");

		assert(star.run(stringToAccept)); //true
		assert(star.run(stringToAccept+stringToAccept)); //true
		assert(!star.run(stringToAccept+"S")); //false

		return star;
	}

	/**
	 * Tests the union operator..
	 * 
	 * Automaton c = a | b;
	 * 
	 * @param alpha 
	 * @param beta
	 * @param alphaAcceptString a string that alpha accepts
	 * @param betaAcceptString a string that beta accepts
	 */
	public Automaton testUnion(Automaton alpha, Automaton beta, String alphaAcceptString, 
			String betaAcceptString) {

		Automaton union = new Automaton(alpha.name + beta.name + "union", 
				alpha, beta, "union");


		assert(union.run(alphaAcceptString)); //true
		assert(union.run(betaAcceptString)); //true
		assert(!union.run(alphaAcceptString+"S")); //false

		return union;
	}

	/**
	 * Tests the concatenate operator.
	 * 
	 * Automaton d = a + b; 
	 * 
	 * @param alpha 
	 * @param beta
	 * @param alphaAcceptString a string that alpha accepts
	 * @param betaAcceptString a string that beta accepts
	 */
	public Automaton testConcatenate(Automaton alpha, Automaton beta, String alphaAcceptString, 
			String betaAcceptString) {

		Automaton concatenate = new Automaton(alpha.name + beta.name + "concatenate", 
				alpha, beta, "concatenate");


		assert(concatenate.run(alphaAcceptString+betaAcceptString)); //accept
		assert(!concatenate.run(betaAcceptString)); //reject
		assert(!concatenate.run(alphaAcceptString)); //reject

		return concatenate;
	}



	//what the below source code should translate to
	public void conversion(StateSet t, ALSet all, Automaton test) {
		for (String ch : test.alphabet) {
			StateSet u = new StateSet();
			for (State state : t) {
				StateSet reachable = state.getTransitionStates(ch);
				for (State s : reachable) {
					u = StateSet.union(u, test.epsilonClosure(s));
				}
			}
			if (!u.isEmpty()) {
				//add transition to ignores empty u as well
				t.addTransitionTo(u, ch);
				if (!all.contains(u)) {
					all.add(u);
					conversion(u, all, test);
				}
			}
		}


	}



	/**
	 * # Recursively computes subset construction
	 * Def void NFAConversion(StateSet t, Set all, Automaton m) {
			for each ch in m.alphabet {
				StateSet u;		
				for each state in t {	# StateSet for loop
					StateSet reachable = state(ch);
					for each st in reachable {
						u = u & epsilonClosure(st, m);		# StateSet union
					}
				}
				if (!u.isEmpty()) {			
					t.trans(ch) = u;
					if (!all.contains(u)) {
						all = all & {u};				# Set union
						NFAConversion(u, all, m);
					}
				}
			}
		}
		
		Def Automaton NFAtoDFA(Automaton m) {
			StateSet starting = epsilonClosure(m.start, m);
			Set all = {starting};
			NFAConversion(starting, all, m); # recursively computes subset construction	
		
			# Use built in function to create a DFA from the StateSets and transitions found
			Automaton dfa = makeDFAFromStateSets(all, m, starting);
			return dfa;
		}
	 * 
	 */
	public Automaton makeDFAfromNFAtester() {
		
		State A = new State("A");
		State B = new State("B");
		State C = new State("C");
		State D = new State("D");
		StateSet states = new StateSet(A, B, C, D);
		StateSet accept = new StateSet(B);
		Alphabet alph = new Alphabet("a", "b");
		A.addTransitionStateNoOverwrite(B, "epsilon");
		B.addTransitionStateNoOverwrite(B, "a");
		B.addTransitionStateNoOverwrite(C, "a");
		C.addTransitionStateNoOverwrite(D, "b");
		D.addTransitionStateNoOverwrite(B, "epsilon");
		Automaton test = new Automaton("tester", states, A, accept, alph);

		test.printAutomaton();

		StateSet starting = test.epsilonClosure(test.start);
		ALSet all = new ALSet(starting);
		conversion(starting, all, test);
		Automaton dfa = Automaton.makeDFAFromStateSets("dfa", all, 
				test);

		//dfa.printAutomaton();
		//if (test.run("aab"))
		test.printResult("aabb");	
		dfa.printResult("aabb");

		return test;
	}
	
	/**
	 * Tests example in Language Tutorial with for loops:
	 * Source:
	 * Automaton a = automaton("hey");
	 * Automaton b = automaton("hey");
	 * for(10) {
	 * 		a = a + b;
	 * }
	 * 
	 */
	public Automaton heyHeyTester() {
		
		Automaton a = Automaton.makeAutomatonFor("a", "hey");
		Automaton b = Automaton.makeAutomatonFor("b", "hey");
		for (int i = 0; i < 10; i++) {
			a = Automaton.concatenation("a", a, b);
		}
		a.printResult("heyheyheyheyheyheyheyheyheyheyhey");
		//ALL WORKING!
		return a;
	}

	/**	
	 * 1.	Automaton atom = automaton(“fail”);
	 * 2.	if (atom.run(“fail”)) {
	 * 3.		atom = automaton(“success”);
	 * 4.	} else {
	 * 5.		atom = automaton(“double”) + atom;
	 * 6.	}
	 */
	public Automaton doubleFailTest() {
		Automaton atom = Automaton.makeAutomatonFor("atom", "fail");
		if (atom.run("fail")) {
			atom = Automaton.makeAutomatonFor("atom", "success");
		} else {
			atom = Automaton.concatenation("atom", Automaton.makeAutomatonFor("atom", "double"), atom);
		}
		
		atom.printResult("success");
		return atom;
	}
	
	
	public void test3() { 
			StateSet states;
			StateSet accept;
			Alphabet alphabet;
			State start;
			Automaton test = Automaton.makeAutomatonFor("test", "abcd");		
			Automaton starTest = Automaton.star("starTest", null);		
			
			if (starTest.run("abcdabcd"))
			{
				System.out.println("Success");
			}
			else 
			{
				System.out.println("Failure");
			}
			if (!starTest.run("abcab"))
			{
				System.out.println("Success");
			}
			else 
			{
				System.out.println("Failure");
			}

		}
	
	
	
	/**
	 * Sandbox for testing automaton classes and creation
	 * 
	 */
	public static void main(String[] args) {

		
		AutomatonTester test = new AutomatonTester();



		//TestC should be declared and defined, testA and testB are only declared
		Automaton testA, testB, testC = Automaton.makeAutomatonFor("testC", "lol");
		testC.printAutomaton();
//		testA.printAutomaton();
//		testB.printAutomaton();

		Automaton m = test.myAhoMachine();
		
		//test anonymous automaton given name "temp"
		Automaton p = Automaton.concatenation("p", Automaton.star("temp", m), Automaton.star("temp", m));
		p.printAutomaton();
		
		//testing COPY construction
		//Automaton n = m; in source
	
		m.printAutomaton();
		m.printResult("hah");
		Automaton n = new Automaton("n",m);
		n.printResult("aho");
		
		test.testStateSets();
		test.doubleFailTest();
		
		Automaton nn = n;
		nn.printResult("aho");
		System.out.println(nn.start.equals(n.start));
		
		test.testUnioningTransitions();
	}


}
