/**
 * Class StateSet is an object used in a finite 
 * automaton to represent a collection of machine states.
 * StateSets contain State objects.
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Dianna Hohensee
 * @author Kristie Howard
 * Programming Languages and Translators
 *
 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

import java.util.*;

public class StateSet implements Iterable<State>{

	private TreeMap<String, State> states;			

	//used for NFA to DFA conversion
	public HashMap<String, StateSet> transitions;
	public State tag = null;

	/**
	 * Constructor for a StateSet that initializes the StateSet with 
	 * a variable number of State objects.
	 * 
	 * The resulting StateSet contains references to every State passed
	 * in as an argument
	 * 
	 * @param states a variable number of State objects to be added to this StateSet
	 */
	public StateSet(State... states) 
	{
		this.states = new TreeMap<String, State>();
		if (states.length != 0) {
			for (State s : states) {
				this.states.put(s.getName(), s);
			}
		}
		this.transitions = new HashMap<String, StateSet>();
	}


	/**
	 * This constructor creates a StateSet from two given 
	 * StateSet objects. It is used for the special cases of unioning or 
	 * concatenating Automatons, and will create renamed versions of each state.
	 * 
	 * If StateSet a belonged to an automaton with name aName and had a state
	 * S1 with a transition on 'b' to S2, the resulting StateSet post constructor
	 * will contain a state aNameS1 with a transition on 'b' to aNameS2.
	 *
	 * POST: The resulting StateSet contains COPIES of the original StateSets a and b,
	 * where the copied States have been renamed as automatonName + original state name.
	 * These copied states have replicated transitions to the copied, renamed equivalents of
	 * their original transition targets.  
	 * 
	 * @param a a StateSet
	 * @param b a StateSet
	 * @param aName name of the Automaton in which the StateSet a exists
	 * @param bName name of the Automaton in which the StateSet b exists
	 * @param operator String which should be "union"
	 * 
	 * @throws an IllegalArgumentException if the operator argument is not "union"
	 */
	public StateSet(StateSet a, StateSet b, String aName, String bName, String operator) throws IllegalArgumentException
	{
		this.states = new TreeMap<String, State>();
		if (operator.equals("union")) {
			union(a, b, aName, bName);
		} else {
			throw new IllegalArgumentException("StateSet: Invalid operator String was attempted.\n");
		}
	}


	/**
	 * Copy constructor for a StateSet that iterates through the States in StateSet
	 * a, renames them using the prefix, and replicates their transitions.
	 * 
	 * If StateSet a belonged to an automaton with name prefix and had a state
	 * S1 with a transition on 'b' to S2, the resulting StateSet post constructor
	 * will contain a state prefixS1 with a transition on 'b' to prefixS2.
	 *
	 * POST: The resulting StateSet contains COPIES of the original States in StateSet a,
	 * where the copied States have been renamed as automatonName + original state name.
	 * These copied states have replicated transitions to the copied, renamed equivalents of
	 * their original transition targets. 
	 * 
	 * @param a the StateSet to create a copy of with renamed States
	 * @param prefix the String name of the automaton that StateSet a belongs to
	 */
	public StateSet(StateSet a, String prefix) { 
		this.states = new TreeMap<String, State>();
		addStateSetCopy(a, prefix);
	}


	/**
	 * Adds the given State object by reference to this StateSet. 
	 * 
	 * @param state the State to add to this StateSet
	 */
	public void addState(State state)
	{ 
		if (state != null) 
			states.put(state.getName(), state);
	}	

	/**
	 * Add the given StateSet's States by reference to this StateSet.
	 * 
	 * NOTE: If any State object in StateSet s1 and this 
	 * StateSet have the same key, the StateSet s1's State 
	 * will be stored and the other discarded
	 * 
	 * @param s1 the StateSet object from which States are being added
	 */
	public void addStateSet(StateSet s1) {
		for (State state : s1) {
			states.put(state.getName(), state);
		}
	}


	/**
	 * Adds renamed copies of the States in StateSet a to this StateSet.
	 * 
	 * If StateSet a belonged to an automaton with name prefix and had a state
	 * S1 with a transition on 'b' to S2, the resulting StateSet
	 * will contain a state prefixS1 with a transition on 'b' to prefixS2.
	 * 
	 * POST: This StateSet contains COPIES of the original States in StateSet a,
	 * where the copied States have been renamed as prefix + original state name.
	 * These copied states have replicated transitions to the copied, renamed equivalents of
	 * their original transition targets.  
	 * 
	 * @param a the StateSet to create a copy of with renamed States
	 * @param prefix the String name of the automaton that StateSet a belongs to
	 * 
	 * 
	 * NOTE: If any State objects in StateSet set and this 
	 * StateSet have the same name key, one of the states 
	 * with identical name keys will be kept and the other not.
	 * 
	 */
	public void addStateSetCopy(StateSet a, String prefix) {
		/* Create new states with the prefix */
		for (State state : a) {
			this.addState(new State(prefix + state.getName()));
		}

		/* Add transitions to the new states */
		for (State state : a) {
			// Get the local version of the state
			State t = getStateByName(prefix + state.getName());
			// Get state's transitions
			HashMap<String,StateSet> transitions = state.getAllTransitions();
			// Iterate through state's transitions by key
			for (String k : transitions.keySet()) {
				StateSet set = new StateSet();
				// Iterate through state's StateSet on this key
				// and add states to our version's transition StateSet for this key
				for (State s: state.getTransitionStates(k)) {
					set.addState(getStateByName(prefix + s.getName()));
				}
				t.addTransitionsTo(set, k);
			}
		}
	}

	
	
	/**
	 * This helper method creates a StateSet from two given 
	 * StateSet objects. It is used for the special cases of unioning or 
	 * concatenating Automatons, and will create renamed versions of each state.
	 * 
	 * If StateSet a belonged to an automaton with name aName and had a state
	 * S1 with a transition on 'b' to S2, the resulting StateSet post constructor
	 * will contain a state aNameS1 with a transition on 'b' to aNameS2.
	 *
	 * NOTE: If any State objects in StateSet a and b have
	 * the same name, one of the states with identical name keys
	 * will be kept and the other not.
	 *
	 * @param a a StateSet
	 * @param b a StateSet
	 * @param aName name of the Automaton in which the StateSet a exists
	 * @param bName name of the Automaton in which the StateSet b exists
	 * 
	 */
	private void union(StateSet a, StateSet b, String aName, String bName) {
		// Iterate through the States in the given StateSets a and b,
		// adding copies of the States to this StateSet
		addStateSetCopy(a, aName);
		addStateSetCopy(b, bName);
	}


	/**
	 * Returns the State object in this StateSet corresponding to the given stateName
	 * or null if no such State exists.
	 * 
	 * @param stateName the String name of the State to look for in this StateSet
	 * @return the State in this StateSet with the name stateName, or null
	 * if that State does not exist in that StateSet
	 */
	public State getStateByName(String stateName) {
		return states.get(stateName);
	}



	/**
	 * Returns the StateSet's internal TreeMap<String, States>.
	 * 
	 * @return the TreeMap<String, States> containing all of the State information
	 */
	public TreeMap<String, State> getStates() 
	{
		return states;
	}


	/**
	 * Determines whether this StateSet contains a State
	 * with the given name. 
	 * 
	 * @param stateName the String representing the State object's name
	 * @return true if the internal states TreeMap has the given key (stateName), false otherwise
	 */
	public boolean hasState(String stateName) {
		return states.containsKey(stateName);
	}

	/**
	 * Determines whether this StateSet contains a specific State object
	 * 
	 * @param s the State object to check membership for
	 * @return true if the internal states TreeMap has the given State object, false otherwise
	 */
	public boolean contains(State s) {
		return states.containsValue(s);
	}
	
	/**
	 * Clears this StateSet. Used in the run() function.
	 * 
	 * Post: This StateSet's map has no entries. This StateSet contains no states.
	 */		
	public void clearEntries() {
		states.clear();
	}


	/**
	 * Determine whether or not this StateSet contains any States.
	 * 
	 * @return true if the StateSet is empty, false otherwise
	 */
	public boolean isEmpty() {
		return (states.size() == 0);
	}



	/**
	 * Iterator for the enhanced for-loop capabilities.
	 * 
	 */
	@Override
	public Iterator<State> iterator() {
		Collection<State> allStates = states.values();
		return allStates.iterator();
	}

	/**
	 * Compare two StateSets. Return true if the StateSet
	 * is the same size and every element is equal.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		if (this.states.size() != ((StateSet)obj).states.size())
			return false;
		for (State s : this) { //for every state in this, make sure obj contains it
			if ( !((StateSet)obj).contains(s) )
				return false;
		}

		return true; //same size, all objects are equal (same reference)
	}	


	/**
	 * toString method for StateSet. If StateSet s has 3 States named
	 * "a", "b" and "c", s.toString() returns "{a, b, c}"
	 */
	@Override 
	public String toString() {
		if (this.states.isEmpty()) 
			return "{}";

		String result = "{";
		for (State state : this) {
			result = result + state.getName() + ", ";
		}
		result = result.substring(0, result.length()-2);
		result = result + "}";
		return result;
	}

	/**
	 * Static helper method to union two state sets (by reference without renaming).
	 * Returns an empty StateSet if a and b are both null.
	 * 
	 * @param a one of the two StateSets to perform the operator on
	 * @param b the other one of the two StateSets to perform the operator on
	 * @return the StateSet with the result of a U b
	 */
	public static StateSet union(StateSet a, StateSet b) {
		StateSet temp = new StateSet();
		if (a != null)
			temp.addStateSet(a);
		if (b != null)
			temp.addStateSet(b);
		return temp;
	}


	/**
	 * Static helper method to intersect two state sets (by reference without renaming).
	 * 
	 * @param a one of the two StateSets to perform the operator on
	 * @param b the other one of the two StateSets to perform the operator on
	 * @return the StateSet with the result of a ^ b
	 */
	public static StateSet intersection(StateSet a, StateSet b) {
		StateSet temp = new StateSet(); //CHECK FOR NULL
		for (State s : a) {
			if (b.states.containsValue(s)) {
				temp.addState(s);
			}
		}
		return temp;
	}

	/**
	 * Static helper method to take the set difference of two state sets 
	 * (by reference without renaming).
	 * 
	 * @param a one of the two StateSets to perform the operator on
	 * @param b the other one of the two StateSets to perform the operator on
	 * @return the StateSet with the result of a - b
	 */
	public static StateSet difference(StateSet a, StateSet b) {
		StateSet temp = new StateSet();
		for (State s : a) {
			//only want states in a that b does not contain
			if (!b.states.containsValue(s)) {
				temp.addState(s);
			}
		}
		return temp;
	}


	//NFA TO DFA CONVERSION METHODS ADDED BELOW

	/**
	 * Used for NFA to DFA conversion. 
	 * 
	 * Add ONE transition on the given transChar to the given 
	 * target StateSet object.
	 * 
	 * If a transition already exists for this StateSet on the given transChar, 
	 * the old target StateSet will be replaced with the new one. 
	 * 
	 * @param target the StateSet which this StateSet transitions to on transChar
	 * @param transChar the String character that this transition will be created on
	 * 
	 */
	public void addTransitionsTo(StateSet target, String transChar) 
	{

		if (!target.isEmpty()) {
			transitions.put(transChar, target);

			
//			System.out.println("\nStateSet Transition added from " + 
//					this.toString() + " to "+ 
//					target.toString() + 
//					" on " + transChar);
		}
	}

	/**
	 * Check if this StateSet contains a state that is in
	 * the StateSet acceptingStates.
	 * 
	 * @param acceptingStates the StateSet of accepting states from an Automaton
	 * @return true if this StateSet contains a state in acceptingStates
	 */
	public boolean isAccepting(StateSet acceptingStates) {
		//for each state in this StateSet, is it in the acceptingStates
		for (State s : this) {
			//if yes, return true
			if (acceptingStates.hasState(s.getName()))
				return true;
		}
		return false;
	}

}