/**
 * Class Alphabet is an object used in a finite
 * automaton to represent the alphabet for the machine.
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Dianna Hohensee
 * @author Kristie Howard
 * Programming Languages and Translators

 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

import java.util.*;

public class Alphabet implements Iterable<String>{

	private ArrayList<String> alphabet;
	
	/**
	 * Constructor for an alphabet. Takes a variable number 
	 * of String arguments representing the characters to be in the
	 * alphabet. 
	 * 
	 * @param chars one-letter Strings representing the characters to
	 * 			be in the Alphabet
	 */
	public Alphabet(String... chars) 
	{
		this.alphabet = new ArrayList<String>();
		for (String s : chars) {
			if (!alphabet.contains(s))
				this.alphabet.add(s);
		}
	}
	
	
	/**
	 * Constructor for an alphabet that contains all of the characters
	 * (represented as Strings) in the Alphabet(s) specified in the 
	 * argument(s). 
	 * 
	 * POST: The created Alphabet contains every character in all of the
	 * alphabets passed in as arguments
	 * 
	 * @param alphabets a variable number of Alphabet objects
	 * 
	 */
	public Alphabet(Alphabet... alphabets) {
		this.alphabet = new ArrayList<String>();
		for (Alphabet a : alphabets) {
			for (String s : a.getAlphabet()) {
				if (!alphabet.contains(s))
					alphabet.add(s);
			}
		}
	}
	
	/**
	 * Add the character c to this alphabet.
	 * 
	 * @param c the String character to add to the alphabet
	 */
	public void addChar(String c) {
		if (!alphabet.contains(c))
			alphabet.add(c);
	}
	
	/**
	 * Removes the character c from this alphabet.
	 * 
	 * @param c the String character to remove from the alphabet
	 */
	public void removeChar(String c) {
		if (alphabet.contains(c))
			alphabet.remove(c);
	}

	/**
	 * Check if the character c is in this alphabet.
	 * 
	 * @param c the String representing the character to check membership for
	 * @return boolean specifying if this alphabet contains the character c
	 */
	public boolean contains(String c) {
		return alphabet.contains(c);
	}
	
	
	/**
	 * Returns the alphabet's internal ArrayList<String>.
	 * 
	 * @return the alphabet ArrayList<String>
	 */
	public ArrayList<String> getAlphabet() {
		return alphabet;
	}

	/**
	 * toString() method for representing the character contents
	 * of this Alphabet as a string
	 */
	@Override
	public String toString() {
		String result = "{";
		for (String s : this.alphabet) {
			result = result + s + ", ";
		}
		result = result.substring(0, result.length()-2);
		result = result + "}";
		return result;
	}
	
	/**
	 * iterator() method to allow enhanced for-loops to iterate
	 * over the characters (Strings) in this Alphabet
	 */
	@Override
	public Iterator<String> iterator() {
		return alphabet.iterator();
	}
	
}
