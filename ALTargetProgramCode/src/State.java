/**
 * Class State is an object used in a finite 
 * automaton to represent a state. States hold
 * the transition information for an Automaton.
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Dianna Hohensee
 * @author Kristie Howard
 * Programming Languages and Translators

 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

import java.util.*;

public class State {
	
	private String name;
	private HashMap<String, StateSet> transitions;
	
	
	/**
	 * Creates a state with a given name and an empty transitions
	 * hashmap. 
	 * 
	 * POST: The created state has a name but no transitions
	 * @param stateName the String name of the State
	 */
	public State(String stateName) 
	{
		name = stateName;
		transitions = new HashMap<String, StateSet>();
	}


	/**
	 * Add transitions on a given character from this State to every 
	 * State in the given StateSet.
	 * 
	 * If a transition already exists for this State on the given transChar, 
	 * the old target state(s) will be replaced with the new ones. 
	 * 
	 * POST: This State's transition hashmap has been updated to include 
	 * transitions on transChar to every State in targets. If there were 
	 * previously defined transitions on this character, they will be 
	 * replaced with the new ones. 
	 * 
	 * @param targets the StateSet with all of the target States that transitions
	 * 			will be created to
	 * @param transChar the String character for which transitions will be created on
	 * 
	 */
	public void addTransitionsTo(StateSet targets, String transChar) 
	{
		transitions.put(transChar, targets);
		//NOTES: does not copy StateSet, just adds it.
	
	}

	
	/**
	 * Add a transition on the given transChar to the given 
	 * target State object.
	 * 
	 * If a transition already exists for this State on the given transChar, 
	 * the old target state(s) will be replaced with the new one. 
	 * 
	 * POST: This State's transition hashmap has been updated to have a 
	 * transition on transChar to State target. If there were 
	 * previously defined transitions on this character, they will be 
	 * replaced with the new one. 
	 * 
	 * @param target the State which this State transitions to on transChar
	 * @param transChar the String character that this transition will be created on
	 * 
	 */
	public void addTransitionTo(State target, String transChar) 
	{
		StateSet targets = new StateSet(target);
		transitions.put(transChar, targets);

	}
	
	/**
	 * Removes the transition from this state on the character.
	 * Eliminates that entry in this state's hashmap.
	 * 
	 * @param character the String character to remove all transitions for
	 */
	public void removeTransition(String character) {
		if (this.transitions.containsKey(character)) {
			transitions.remove(character);
		}
	}
	
	
	
	
	/**
	 * Add a transition on the given transChar from this State to the given 
	 * target State.
	 * 
	 * If a transition already exists for this State on the given transChar, 
	 * the new one will be ADDED to the old target state(s). This is a non-overwriting
	 * version of addTransitionTo(State target, String transChar).
	 * 
	 * POST: This State's transition hashmap has been updated to have a 
	 * transition on transChar to State target. If there were 
	 * previously defined transitions on this character, they will still remain
	 * in the transition hashmap. 
	 * 
	 * @param target the State which this State transitions to on transChar
	 * @param transChar the String character that this transition will be created on
	 * 
	 */
	public void addTransitionStateNoOverwrite(State target, String transChar) {
		if (transitions.containsKey(transChar)) {
			// Get the transition StateSet and add the new State
			StateSet s = transitions.get(transChar);
			s.addState(target);
		} else {
			// Create a new transition for this State
			this.addTransitionTo(target, transChar);
		}
	}
	
	
	/**
	 * Returns the StateSet representing the states that this State transitions
	 * to on character(s) transChars.
	 * 
	 * If no transition exists on transChar from this State, an empty StateSet will
	 * be returned.
	 * 
	 * @param transChars the variable number of String characters to search for transitions on
	 * @return the StateSet containing the target transition states on the character
	 * 		transChar, or an empty StateSet if no transitions exist on transChar(s)
	 */
	public StateSet getTransitionStates(String... transChars)
	{
		StateSet ret = new StateSet();
		for (String t : transChars) {
			StateSet s = transitions.get(t);
			if (s != null)
				ret.addStateSet(s);
		}
		
		return ret;
	}

	
	/**
	 * Returns the HashMap<String, StateSet> containing all of the
	 * transitions for this State.
	 */
	public HashMap<String,StateSet> getAllTransitions() {
		return this.transitions;
	}

	
	/**
	 * Returns the name of this State.
	 * 
	 * @return the String name of this state
	 */
	public String getName() 
	{
		return name;
	}
	
}
