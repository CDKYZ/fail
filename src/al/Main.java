/**
 * Main runs the translator.
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Yujin Ariza
 * @author Christopher Erlendson
 * Programming Languages and Translators
 *
 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

package al;
import al.parser.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.io.*;

public class Main
{
	public static void main(String[] args) {

		String file = "tests/print.al";
		if (args.length > 0) {
			file = args[0];
		}

		String outfile = file.replaceFirst("\\.al$", ".java");
		System.out.println("Outfile: " + outfile);

		String classname = file.replaceAll("[^/]*/", "");
		classname = classname.replaceFirst("\\.al$", "");

		// create an instance of the lexer

		try {
			ANTLRFileStream input;
			input = new ANTLRFileStream(file);

			ALLexer lexer = new ALLexer(input);
			    
			// wrap a token-stream around the lexer
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			    
			// create the parser
			ALParser parser = new ALParser(tokens);

			// generate AST
		    ParseTree tree = parser.start();
		    // String t = tree.toStringTree();
		    // System.out.println(t);

		    ParseTreeWalker walker = new ParseTreeWalker();

		    // semantic checks
    		DefPhase def = new DefPhase();
    		walker.walk(def, tree);

    		// target code generation
		    // CodeGenListener codeGen = new CodeGenListener(parser, classname);
		    // walker.walk(codeGen, tree);

		    // System.out.println("CODEGEN: \n" + codeGen.output);

		    CodeGenVisitor codeGen = new CodeGenVisitor(parser, classname);
		    String code = codeGen.visit(tree);

		    // write codeGen.output to file
			PrintWriter writer;
			try {
				writer = new PrintWriter(outfile, "UTF-8");
				writer.println(code);
				writer.close();
			} catch (FileNotFoundException f) {
				// TODO Auto-generated catch block
				f.printStackTrace();
			} catch (UnsupportedEncodingException f) {
				// TODO Auto-generated catch block
				f.printStackTrace();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}


	}
}
