/**
 * Subclass of Symbol.
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Christopher Erlendson
 * Programming Languages and Translators
 *
 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

package al.parser;
import al.parser.*;
/** Represents a variable definition (name,type) in symbol table */
public class VariableSymbol extends Symbol {
    public VariableSymbol(String name, Type type) { super(name, type); }
}
