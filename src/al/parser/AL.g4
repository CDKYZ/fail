/**
 * AL Grammar. The G4 file is compiled into .java code by the ANTLRv4 library.
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Christopher Erlendson
 * @author Yujin Ariza
 * @author Dianna Hohensee
 * Programming Languages and Translators
 *
 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */


grammar AL;

@header {
   package al.parser;
   import al.parser.*;
}

/* Rules */

start      
       : statementList EOF
       ;


/* Statement rules */

statementList     
                    : statement
                    | statementList statement
                    ;

statement
                    : expressionStatement
                    | compoundStatement
                    | selectionStatement
                    | iterationStatement
                    | functionStatement
                    | returnStatement
                    | declaration
                    | automatonDecl
                    | functionDefinition
                    ;

expressionStatement
                     : expression ';'
                     ;

compoundStatement   
                     : OPENBRACKET statementList CLOSEBRACKET
                     ;

selectionStatement
                     : IF OPENPAREN ifnot='!'? ifexp=booleanFunction CLOSEPAREN ifstat=compoundStatement
                     | IF OPENPAREN ifnot='!'? ifexp=booleanFunction CLOSEPAREN ifstat=compoundStatement 
                       (ELSEIF OPENPAREN elifnot='!'? elifexp=booleanFunction CLOSEPAREN elifstat=compoundStatement)* 
                       ELSE elsestat=compoundStatement
                     ;
 
iterationStatement
                     : 'for each' (typeSpecifier|DECLCHAR) ID 'in' (twoLevel | functionExpressionWithReturn) compoundStatement
                     | 'for' OPENPAREN NUM CLOSEPAREN compoundStatement
                     ;

functionStatement
                     : booleanFunction ';'
                     | functionExpression ';'
                     ;

returnStatement
                     : 'return' logicalFirstExpression ';'        /* This really correct? */
                     ;







/* Automaton rules */

automatonDecl 
                  : 'Automaton' ID OPENBRACKET declStates autoStatesDecl autoStartDecl autoAcceptDecl
                                       autoAlphabetDecl transitionList CLOSEBRACKET ';'
                  ;

declStates        
                  : ('State' idList)+ ';'
                  ;

idList            
                  : ID
                  | idList ',' ID
                  ;

autoStatesDecl  
                  : 'StateSet' ID '=' set ';'
                  ;

autoStartDecl   
                  : 'State' ID '=' ID ';'
                  ;

autoAcceptDecl  
                  : 'StateSet' ID '=' set ';'
                  ;

autoAlphabetDecl        
                  : 'Alphabet' ID '=' charSet ';'
                  ;

transitionList  
                  : (transition)*
                  ;







/* Function rules */

functionDefinition
                               : 'Def' (typeSpecifier|VOID) ID OPENPAREN declParameterList? CLOSEPAREN compoundStatement
                               ;

booleanFunction
                               : contains
                               | isempty 
                               | run                                
                               ;

functionExpression
                               : print
                               | printResult
                               | autoPrint
                               | removeTrans
                               | functionExpressionWithReturn
                               ;

functionExpressionWithReturn
                               : trans
                               | eclosure
                               | automaton
                               | makeDFAFromStateSets
                               | generalFunction
                               ;

contains
                               : twoLevel '.' CONTAINS OPENPAREN parameter CLOSEPAREN                     /* StateSet/Set     ID, ID'.'ID */
                               ;
isempty
                               : twoLevel '.' ISEMPTY OPENPAREN CLOSEPAREN                                   /* StateSet/Set     ID, ID'.'ID */
                               ;
run
                               : ID '.' RUN OPENPAREN STRING CLOSEPAREN                                   /* Automaton */
                               ;

print
                               : PRINT OPENPAREN STRING CLOSEPAREN 
                               | PRINT OPENPAREN ID CLOSEPAREN
                               ;

printResult
                               : PRINTRESULT OPENPAREN parameter ',' STRING CLOSEPAREN  
                               ;

autoPrint
                               : ID '.' PRINT OPENPAREN CLOSEPAREN                                           /* Automaton */
                               ;

removeTrans
                               : threeLevel '.' REMOVETRANS OPENPAREN charList CLOSEPAREN                 /* StateSet/State ID, ID'.'ID, ID'.'ID'.'ID' */
                               ;

trans
                               : threeLevel '.' TRANS OPENPAREN (charList|ID) CLOSEPAREN                  /* StateSet/State ID, ID'.'ID, ID'.'ID'.'ID' */
                               ;

eclosure
                               : ID '.' ECLOSURE OPENPAREN threeLevel CLOSEPAREN                          /* Automaton */
                               ;

automaton
                               : AUTOMATON OPENPAREN STRING CLOSEPAREN 
                               ;

makeDFAFromStateSets   
                               : MAKEDFAFROMSTATESETS OPENPAREN parameter ',' parameter CLOSEPAREN
                               ;

generalFunction
                               : ID OPENPAREN parameterList? CLOSEPAREN  
                               ;

twoLevel                       
                               : ID
                               | ID '.' ID
                               ;

threeLevel                     
                               : ID
                               | ID '.' ID
                               | ID '.' ID '.' ID
                               ;

declParameterList
                               : declParameter 
                               | declParameterList ',' declParameter
                               ;

declParameter
                               : typeSpecifier ID
                               ;

parameterList
                               : parameter
                               | parameterList ',' parameter
                               ;

parameter
                               : ID
                               | STRING
                               | logicalFirstExpression
                               ;







/* Declaration rules */

declaration
                    : typeSpecifier initDeclaratorList ';'
                    ;

initDeclaratorList
                    : initDeclarator
                    | initDeclaratorList ',' initDeclarator
                    ;

initDeclarator
                    : declarator
                    | declarator '=' initializer
                    ;

declarator
                    : ID
                    ;

initializer
                    : expression
                    ;







/* Expression rules */

expression
                         : assignmentExpression
                         ;

automatonExpression      
                         : first=ID '.' second=ID
                         | first=ID '.' second=ID '.' third=ID
                         ;

assignmentExpression
                         : ID '=' assignmentExpression
                         | trans '=' assignmentExpression
                         | logicalFirstExpression
                         ;

logicalFirstExpression
                         : logicalSecondExpression
                         | logicalFirstExpression UNION logicalSecondExpression
                         | logicalFirstExpression SETUNION logicalSecondExpression
                         ;

SETUNION : '&';

UNION
                         : '|'
                         ;

logicalSecondExpression
                         : logicalThirdExpression
                         | logicalSecondExpression INTERSECTION logicalThirdExpression
                         | logicalSecondExpression CONCATENATION logicalThirdExpression
                         ;

INTERSECTION : '^';

CONCATENATION
                         : '+'
                         ;

logicalThirdExpression
                         : logicalFourthExpression
                         | logicalFourthExpression KLEENE_STAR
                         | logicalThirdExpression DIFFERENCE logicalFourthExpression
                         ;

DIFFERENCE : '-';

KLEENE_STAR
                         : '*'
                         ; 

logicalFourthExpression
                         : primaryExpression
                         | OPENPAREN logicalFirstExpression CLOSEPAREN
                         ;

primaryExpression
                         : ID
                         | set
                         | charSet
                         | automatonExpression
                         | functionExpressionWithReturn
                         ;







/* Basic rules */

transition      
               : ID '.' TRANS OPENPAREN charList CLOSEPAREN '=' set ';'
               ;

set        
               : OPENBRACKET list CLOSEBRACKET
               ;

list
               : ID
               | list ',' ID
               ;

charSet
               : OPENBRACKET charList CLOSEBRACKET
               ;

charList
               : (EPSILON|CHAR|letters|numbers)
               | charList ',' (EPSILON|CHAR|letters|numbers)
               ;

typeSpecifier
               : 'Set'
               | 'StateSet'
               | 'State'
               | 'Automaton'
               | 'Alphabet'
               ;

letters
               : 'LETTERS'
               ;

numbers
               : 'NUMBERS'
               ;

string
               : STRING
               ;







/* Lexer tokens */

OPENPAREN : '(';

CLOSEPAREN: ')';

OPENBRACKET: '{';

CLOSEBRACKET: '}';

IF : 'if';

ELSE : 'else';

ELSEIF: 'elseif';

VOID
            : 'void'
            ;

CONTAINS    
            : 'contains'
            ;

ISEMPTY     
            : 'isEmpty'
            ;

RUN         
            : 'run'
            ;

PRINT       
            : 'print'
            ;

PRINTRESULT 
            : 'printResult'
            ;

REMOVETRANS 
            : 'removeTrans'
            ;

TRANS       
            : 'trans'
            ;

ECLOSURE    
            : 'eclosure'
            ;

AUTOMATON   
            : 'automaton'
            ;

MAKEDFAFROMSTATESETS
                    : 'makeDFAFromStateSets'
                    ;

DECLCHAR       
            : 'Char'
            ;

COMMENT     
            : '#' (~'\n')* '\n' -> skip
            ;

WHITESPACE
            : (' ' | '\n' | '\t')+ -> skip
            ;

EPSILON
            : '\'epsilon\''
            ;

ID    
            : [A-Za-z]([A-Za-z]|[0-9])*
            ;

CHAR     
            : '\'' ([a-zA-Z0-9]|'!'|'@'|'#'|'$'|'%'|'^'|'&'|'*'|'('|')'|'+'|'-'|'~'|'?'|'/'|'>'|'<'|'_'|'='|'['|']'|'{'|'}'|';'|'.' ) '\''
            ;

STRING    
            : '"' ( ~'"'|'!'|'@'|'#'|'$'|'%'|'^'|'&'|'*'|'('|')'|'+'|'-'|'~'|'?'|'/'|'>'|'<'|'_'|'='|'['|']'|'{'|'}'|';'|'\''|'.' )* '"'
            ;

NUM  
            : [0-9]+
            ;

