/**
 * Symbol class used in Semantic checking
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Christopher Erlendson
 * Programming Languages and Translators
 *
 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

package al.parser;
import al.parser.*;
public class Symbol { // A generic programming language symbol
    public static enum Type {tAutomaton, tAlphabet, tState, tSet, tStateSet, tINVALID}

    public static Type getType(String declaredType) {
        if (declaredType.equals("tAutomaton")) {
            return Type.tAutomaton;
        } else if (declaredType.equals("tAlphabet")) {
            return Type.tState;
        } else if (declaredType.equals("tState")) {
            return Type.tState;
        } else if (declaredType.equals("tSet")) {
            return Type.tSet;
        } else if (declaredType.equals("tStateSet")) {
            return Type.tStateSet;
        } else {
            return Type.tINVALID;
        }
    }

    String name;      // All symbols at least have a name
    Type type;
    Scope scope;      // All symbols know what scope contains them.

    public Symbol(String name) { this.name = name; }
    public Symbol(String name, Type type) { this(name); this.type = type; }
    public String getName() { return name; }

    public String toString() {
        if ( type!=Type.tINVALID ) return '<'+getName()+":"+type+'>';
        return getName();
    }
}
