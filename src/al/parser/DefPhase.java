/**
 * Listener for Scope checking.
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Christopher Erlendson
 * Programming Languages and Translators
 *
 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

package al.parser;
import al.parser.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import java.util.ArrayList;
import java.util.List;

public class DefPhase extends ALBaseListener {
    ParseTreeProperty<Scope> scopes = new ParseTreeProperty<Scope>();
    GlobalScope globals;
    Scope currentScope; // define symbols in this scope

	@Override public void enterStart(ALParser.StartContext ctx){
		globals = new GlobalScope(null);
		currentScope = globals;

	}

	@Override public void exitStart(ALParser.StartContext ctx) {
		System.out.println(globals);
	}

	@Override public void exitAutomatonDecl(ALParser.AutomatonDeclContext ctx) {
		defineVar("tAutomaton", ctx.ID().getSymbol().getText());
	}

    @Override public void exitDeclaration(ALParser.DeclarationContext ctx) {
        /*   Handles arbitrarily long declaration lists
         *   e.g. processes both State s;
         *   and State s, t, u, v,...;
         */
        String type = ctx.typeSpecifier().getText();
        List<String> names = new ArrayList<String>();
        // while there's still a initDeclaratorList child
        ALParser.InitDeclaratorListContext child = ctx.initDeclaratorList();
        while( child != null) {
            names.add(child.initDeclarator().declarator().getText());
            child = child.initDeclaratorList();
        }
        // define all variables in declaration
        for (String name : names) {
            defineVar(type, name);            
        }        

    }

    @Override public void enterFunctionDefinition(ALParser.FunctionDefinitionContext ctx) {
        // get the function type
        String typeText = ""; 
        if (ctx.VOID() != null) {
            typeText = "void";
        } else if (ctx.typeSpecifier() != null) {
            typeText = ctx.typeSpecifier().getText();

        } else {
            // should never come to this as it would be a lexer error, but just in case...
            System.out.println("ERROR! Function definition does not have type specified");
        }
        // get the function name
        String name = ctx.ID().getText();
        Symbol.Type type = Symbol.getType(typeText);
        FunctionSymbol func = new FunctionSymbol(name, type, currentScope);

        // now to put it into the symbol table in current scope
        currentScope.define(func);
        // function's enclosing scope is the old one
        saveScope(ctx, func);
        // current scope is that of the function now
        currentScope = func;

    }

    void defineVar(String declaredType, String name) {
        Symbol.Type type = Symbol.getType(declaredType);
        VariableSymbol var = new VariableSymbol(name, type);
        currentScope.define(var); // Define symbol in current scope
    }

    void saveScope(ParserRuleContext ctx, Scope s) { scopes.put(ctx, s); }

 }
