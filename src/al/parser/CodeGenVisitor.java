/**
 * Returns translated Java output given a parse tree.
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Yujin Ariza
 * @author Christopher Erlendson
 * Programming Languages and Translators
 *
 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

package al.parser;
import al.parser.*;

// Generated from src/main/al/parser/AL.g4 by ANTLR 4.2
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.misc.NotNull;
// copying imports from ALBase
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.*;


public class CodeGenVisitor extends ALBaseVisitor<String>{
	
	String classname;
	ALParser parser;

	int curTab = 2;

	int temp = 1;

	public String tabs() {
		return tabs(curTab);
	}

	public String tabs(int num) {
		String ret = "";
		for (int i=0; i<num; i++) {
			ret += "\t";
		}
		return ret;
	}

	public CodeGenVisitor(ALParser parser, String classname) {
		this.parser = parser;
		this.classname = classname;
	}

	@Override public String visitStart(@NotNull ALParser.StartContext ctx) 
	{ 
		String classHeader = new String(
				"public class " + classname + " { \n");
		String header = new String(
				"\tpublic static void main(String[] args) {\n" +
					"\t\tStateSet states;\n" + 
					"\t\tStateSet accept;\n" + 
					"\t\tAlphabet alphabet;\n" + 
					"\t\tState start;\n");

		// return visitChildren(ctx); 

		String main = "";
		String functions = "";

		ALParser.StatementListContext list = ctx.statementList();
		while (list != null) {
			ALParser.StatementContext stat = list.statement();
			if (stat.functionDefinition() != null) {
				//this is a function
				curTab = 1;
				functions = visitChildren(stat) + functions;
			}
			else {
				curTab = 2;
				main = visitChildren(stat) + main;
			}
			list = list.statementList();
		}

		return classHeader + 
			   header + 
			   main + "\n\t}\n\n" + 
			   functions + "\n}";
	}

	@Override public String visitFunctionDefinition(@NotNull ALParser.FunctionDefinitionContext ctx) {

		String ret = tabs();
		ret += "public static ";
		if (ctx.typeSpecifier() != null) {
			ret += visit(ctx.typeSpecifier());
		}
		else {
			ret += ctx.VOID().getText();
		}
		ret += " " + ctx.ID() + "(";
		String args = "";
		ALParser.DeclParameterListContext list = ctx.declParameterList();
		while(list.declParameterList() != null) {
			// ret += ctx.declParameterList().getText();
			args = ", " + visit(list.declParameter().typeSpecifier()) +
			       " " + list.declParameter().ID().getText() + args;

			list = list.declParameterList();
		}
		args = visit(list.declParameter().typeSpecifier()) +
		       " " + list.declParameter().ID().getText() + args;

		ret += args;
		ret += ")\n";
		ret += visit(ctx.compoundStatement());
		return ret;
	}

	@Override public String visitExpressionStatement(@NotNull ALParser.ExpressionStatementContext ctx) {
		return tabs() + visit(ctx.expression()) + ";\n";
	}

	@Override public String visitTypeSpecifier(@NotNull ALParser.TypeSpecifierContext ctx) 
	{ 
		String res = ctx.getText();
		if (res.equals("Set")) {
			res = "ALSet";
		}
		return res;
	}


	@Override public String visitStatementList(@NotNull ALParser.StatementListContext ctx) {

		String res = "";
		ALParser.StatementListContext list = ctx;
		while (list != null) {
			ALParser.StatementContext stat = list.statement();
			if (stat.functionDefinition() == null) { //no functions
				res = visitChildren(stat) + res;
			}
			list = list.statementList();
		}
		return res;
	}

	@Override public String visitReturnStatement(@NotNull ALParser.ReturnStatementContext ctx) {

		return tabs() + "return " + visit(ctx.logicalFirstExpression()) + ";\n";
	}

	@Override public String visitGeneralFunction(@NotNull ALParser.GeneralFunctionContext ctx) {

		String ret = ctx.ID().getText();
		ret += "(";
		if (ctx.parameterList() != null) {
			ret += visit(ctx.parameterList());
		}
		ret += ")";
		return ret;
	}

	@Override public String visitParameterList(@NotNull ALParser.ParameterListContext ctx) {

		String ret = "";
		while (ctx.parameterList() != null) {
			String param;
			if (ctx.parameter().logicalFirstExpression() != null) {
				param = visit(ctx.parameter());
			}
			else {
				param = ctx.parameter().getText();
			}

			ret = ", " + param + ret;

			ctx = ctx.parameterList();
		}
		if (ctx.parameter().logicalFirstExpression() != null) {
			ret = visit(ctx.parameter()) + ret;
		}
		else {
			ret = ctx.parameter().getText() + ret;			
		}

		return ret;
	}

	@Override public String visitParameter(@NotNull ALParser.ParameterContext ctx)  {

		String ret;

		if (ctx.logicalFirstExpression() != null) {
			ret = visitChildren(ctx);
		}
		else {
			ret = ctx.getText();			
		}
		return ret;
	}


	@Override public String visitAutomatonDecl(@NotNull ALParser.AutomatonDeclContext ctx) {

		String ret = "";

		ret += visit(ctx.declStates());
		ret += visit(ctx.autoStatesDecl());
		ret += visit(ctx.autoStartDecl());
		ret += visit(ctx.autoAcceptDecl());
		ret += visit(ctx.autoAlphabetDecl());
		if (ctx.transitionList() != null) {
			ret += visit(ctx.transitionList());
		}

		ret += tabs() + "Automaton " + ctx.ID() + 
		       " = new Automaton(\"" +
			   ctx.ID() + "\", states, start, accept, alphabet);\n";
		return ret;
	}

	@Override public String visitDeclStates(@NotNull ALParser.DeclStatesContext ctx) {

		String ret = "";

		for (ALParser.IdListContext id : ctx.idList()) {
			// print tabs
			ret += tabs();
			ret += "State " + id.ID() + " = new State(\"" + id.ID() + "\");\n";
			ALParser.IdListContext child = id.idList();
			while (child != null) {
				ret += tabs();
				ret += "State " + child.ID() + " = new State(\"" + child.ID() + "\");\n";
				child = child.idList();
			}
		}
		return ret;
	}

	@Override public String visitAutoStatesDecl(@NotNull ALParser.AutoStatesDeclContext ctx)  {

		String ret = "";

		ret += tabs();
		ret += "states = new StateSet(";
		// get the IDs
		ALParser.ListContext child = ctx.set().list();
		while (child != null) {
			ret += child.ID();
			if (child.list() != null) {
				// we have another ID, add comma and space
				ret += ", ";
			}
			child = child.list();
		}
		ret += ");\n";
		return ret;
	}

	@Override public String visitAutoStartDecl(@NotNull ALParser.AutoStartDeclContext ctx) {

		String ret = "";

		ret += tabs();
		ret += "start = " + ctx.ID().get(1).getText() + ";\n";

		return ret;
	}

	@Override public String visitAutoAcceptDecl(@NotNull ALParser.AutoAcceptDeclContext ctx) {

		String ret = "";

		ret += tabs();
		ret += "accept = new StateSet(";
		// get the IDs
		ALParser.ListContext child = ctx.set().list();
		while (child != null) {
			ret += child.ID();
			if (child.list() != null) {
				// we have another ID, add comma and space
				ret += ", ";
			}
			child = child.list();
		}
		ret += ");\n";

		return ret;
	}

	@Override public String visitAutoAlphabetDecl(@NotNull ALParser.AutoAlphabetDeclContext ctx) {

		String ret = "";

		ret += tabs();
		ret += "alphabet = " + visit(ctx.charSet());
		ret += ";\n";

		return ret;
	}

	@Override public String visitTransitionList(@NotNull ALParser.TransitionListContext ctx) {

		String ret = "";
		if (ctx.children != null) {
			for (ParseTree trans : ctx.children) {
				ret += visit(trans);
			}
		}
		return ret;
	}

	@Override public String visitTransition(@NotNull ALParser.TransitionContext ctx) {

		String ret = "";

		String name = ctx.ID().getText();
		String charListString = ctx.charList().getText();
		charListString = charListString.replaceAll("\'", "\"");
		String[] charListSplit = charListString.split(",|\\s");

		// to find out how many transition chars, need to tokenize
		String[] targetStates = ctx.set().list().getText().split(",|\\s");
		for (String sym : charListSplit) {
			if (sym.equals("LETTERS")) {
				//macros
				String[] symsplit = visitLetters(null).split(",(\\s)*");
				for (String mac : symsplit) {
					for (String targetState : targetStates) {
						ret += tabs();
						ret += name + ".addTransitionStateNoOverwrite(" + targetState + ", " + mac + ");\n";
					}
				}
			}
			else if (sym.equals("NUMBERS")) {
				//macros
				String[] symsplit = visitNumbers(null).split(",(\\s)*");
				for (String mac : symsplit) {
					for (String targetState : targetStates) {
						ret += tabs();
						ret += name + ".addTransitionStateNoOverwrite(" + targetState + ", " + mac + ");\n";
					}
				}	
			}
			else {
				for (String targetState : targetStates) {
					ret += tabs();
					ret += name + ".addTransitionStateNoOverwrite(" + targetState + ", " + sym + ");\n";
				}				
			}
		}
		return ret;
	}

	@Override public String visitBooleanFunction(@NotNull ALParser.BooleanFunctionContext ctx) {
		if (ctx.contains() != null) {
			return ctx.contains().twoLevel().getText() + "." + "contains" + "(" +
					visit(ctx.contains().parameter()) + ")";
		}
		return ctx.getText();
	}

	@Override public String visitFunctionStatement(@NotNull ALParser.FunctionStatementContext ctx) 
	{
		if (ctx.functionExpression() != null) {
			return tabs() + visit(ctx.functionExpression()) + ";\n"; 
		} else if (ctx.booleanFunction() != null) {
			return tabs() + visit(ctx.booleanFunction()) + ";\n";
		} else {
			System.out.println("Error: no match in FunctionStatementContext");
			return "";
		}
	}

	@Override public String visitDeclaration(@NotNull ALParser.DeclarationContext ctx) {
		String res = tabs() + visit(ctx.typeSpecifier()) + " " + visit(ctx.initDeclaratorList()) + ";\n";
		return res;

	}


	@Override public String visitInitDeclaratorList(@NotNull ALParser.InitDeclaratorListContext ctx) {
		String res = "";
		if (ctx.initDeclaratorList() != null) {
			res += visit(ctx.initDeclaratorList()) + ", ";
		}
		res += visit(ctx.initDeclarator());
		return res;
	}

	public String getDeclType(ALParser.InitDeclaratorContext ctx) {

		ParserRuleContext parent = ctx.getParent();
		while (! (parent instanceof ALParser.DeclarationContext)) {
			parent = parent.getParent();
		}
		return visit(((ALParser.DeclarationContext) parent).typeSpecifier());
	}

	@Override public String visitInitDeclarator(@NotNull ALParser.InitDeclaratorContext ctx) 
	{ 
		String res = ctx.declarator().ID().getText();
		if (ctx.initializer() != null ) {
			res += " = " + visit(ctx.initializer());
		}
		else if (getDeclType(ctx).equals("State")) {
			res += " = " + "new State(" + "\"" + extractID(ctx) + "\"" + ")";
		}
		else {
			String type = getDeclType(ctx);
			res += " = " + "new " + type + "()";
		}
		return res;
	}

	public boolean isDirectAssignment(ParserRuleContext ctx)
	{
		ParserRuleContext parent = ctx.getParent();
		while (parent != null) {
			if (parent instanceof ALParser.LogicalThirdExpressionContext || 
				parent instanceof ALParser.LogicalSecondExpressionContext ||
				parent instanceof ALParser.LogicalFirstExpressionContext) {

				if (parent.children.size() > 1) {
					//anonymous variable
					return false;
				}
			}
			if (parent instanceof ALParser.InitDeclaratorContext) {
				return true;
			}
			if (parent instanceof ALParser.AssignmentExpressionContext) {
				return true;
			}
			parent = parent.getParent();	
		}
		return false;
	}

	public String extractID (ParserRuleContext ctx)
	{
		ParserRuleContext parent = ctx.getParent();
		while (parent != null) {
			if (parent instanceof ALParser.LogicalThirdExpressionContext || 
				parent instanceof ALParser.LogicalSecondExpressionContext ||
				parent instanceof ALParser.LogicalFirstExpressionContext) {

				if (parent.children.size() > 1) {
					//anonymous variable
					return "t" + temp++;
				}
			}

			if (parent instanceof ALParser.InitDeclaratorContext) {
				return ((ALParser.InitDeclaratorContext)parent).declarator().ID().getText();
			}
			if (parent instanceof ALParser.AssignmentExpressionContext) {
				if (parent.children.size() > 1) {
					return ((ALParser.AssignmentExpressionContext) parent).getChild(0).getText();
				}
			}
			parent = parent.getParent();
		}
		return "t" + temp++;
	}

	@Override public String visitLogicalFirstExpression(@NotNull ALParser.LogicalFirstExpressionContext ctx) {

		String ret;
		if (ctx.UNION() != null) {
			String id = extractID(ctx);
			ret = "Automaton.union(" + "\"" + id + "\"" + ", " +
					visit(ctx.logicalFirstExpression()) + ", " +
					visit(ctx.logicalSecondExpression()) + ")";
		}
		else if (ctx.SETUNION() != null) {
			ret = "Boilerplate.union(" + 
					visit(ctx.logicalFirstExpression()) + ", " + 
					visit(ctx.logicalSecondExpression()) + ")";
		}
		else {
			ret = visit(ctx.logicalSecondExpression());
		}
		return ret;
	}

	@Override public String visitLogicalSecondExpression(@NotNull ALParser.LogicalSecondExpressionContext ctx) {

		String ret;
		if (ctx.INTERSECTION() != null)	{
			ret = "Boilerplate.intersection(" + 
					visit(ctx.logicalSecondExpression()) + ", " + 
					visit(ctx.logicalThirdExpression()) + ")";
		}
		else if (ctx.CONCATENATION() != null) {
			String id = extractID(ctx);
			ret = "Automaton.concatenation(" + "\"" + id + "\"" + ", " +
				  visit(ctx.logicalSecondExpression()) + ", " + 
				  visit(ctx.logicalThirdExpression()) + ")";
		}
		else {
			ret = visit(ctx.logicalThirdExpression());
		}
		return ret;	
	}

	@Override public String visitLogicalThirdExpression(@NotNull ALParser.LogicalThirdExpressionContext ctx) {

		String ret;
		if (ctx.KLEENE_STAR() != null) {
			String id = extractID(ctx);
			ret = "Automaton.star(" + "\"" + id + "\"" + ", " + 
				  visit(ctx.logicalFourthExpression()) + ")";
		}
		else if (ctx.DIFFERENCE() != null) {
			ret = "Boilerplate.difference(" + 
					visit(ctx.logicalThirdExpression()) + ", " +
					visit(ctx.logicalFourthExpression()) + ")";
		}
		else {
			ret = visit(ctx.logicalFourthExpression());
		}
		return ret;
	}

	@Override public String visitLogicalFourthExpression(@NotNull ALParser.LogicalFourthExpressionContext ctx) {

		if (ctx.primaryExpression() != null)
			return visit(ctx.primaryExpression());
		else
			return visit(ctx.logicalFirstExpression());
	}

	@Override public String visitPrimaryExpression(@NotNull ALParser.PrimaryExpressionContext ctx) {

		if (ctx.ID() != null) {
			if (isDirectAssignment(ctx))
				return "Boilerplate.copy(" + "\"" + extractID(ctx) + "\"" +
				", " + ctx.ID().getText() + ")";
			else
				return ctx.ID().getText();
		}
		else if (ctx.set() != null) {
			return visit(ctx.set());
		}
		else if (ctx.charSet() != null) {
			return visit(ctx.charSet());
		}
		else if (ctx.automatonExpression() != null) {
			return visit(ctx.automatonExpression());
		}
		else
			return visit(ctx.functionExpressionWithReturn());
	}

	// need to ask Dianna about ID()
	@Override public String visitFunctionExpressionWithReturn(@NotNull ALParser.FunctionExpressionWithReturnContext ctx) 
	{
		String res = "";
		if (ctx.trans() != null) {
			String name = visit(ctx.trans().threeLevel());
			res += name + ".getTransitionStates(";

			if (ctx.trans().charList() != null) {
				String charListString = ctx.trans().charList().getText();
				charListString = charListString.replaceAll("\'", "\"");
				res += charListString +")";
			} else if (ctx.trans().ID() != null) {
				res += ctx.trans().ID().getText() + ")";
			}

		}
		else {
			res = visitChildren(ctx);
		}
		return res;
	}

	@Override public String visitSet(@NotNull ALParser.SetContext ctx) {

		String args = ctx.list().getText();
		return "Boilerplate.set(" + args + ")";
	}

	@Override public String visitCharSet(@NotNull ALParser.CharSetContext ctx) {

		String args = visit(ctx.charList());
		return "new Alphabet(" + args + ")";
	}

	@Override public String visitCharList(@NotNull ALParser.CharListContext ctx) {
		String ret = "";
		while (ctx.charList() != null) {
			if (ctx.EPSILON() != null)
				ret = ", " + "\"epsilon\"" + ret;
			else if (ctx.CHAR() != null) 
				ret = ", " + ctx.CHAR().getText().replace('\'', '\"') + ret;
			else if (ctx.letters() != null)
				ret = ", " + visit(ctx.letters()) + ret;
			else
				ret = ", " + visit(ctx.numbers()) + ret;

			ctx = ctx.charList();
		}
		if (ctx.EPSILON() != null)
			ret = "\"epsilon\"" + ret;
		else if (ctx.CHAR() != null) 
			ret = ctx.CHAR().getText().replace('\'', '\"') + ret;
		else if (ctx.letters() != null)
			ret = visit(ctx.letters()) + ret;
		else
			ret = visit(ctx.numbers()) + ret;
		return ret;
	}

	@Override public String visitLetters(@NotNull ALParser.LettersContext ctx) {

		return "\"a\", \"A\",\"b\", \"B\",\"c\", \"C\",\"d\", \"D\",\"e\", \"E\",\"f\", \"F\",\"g\", \"G\",\"h\", \"H\",\"i\", \"I\",\"j\", \"J\",\"k\", \"K\",\"l\", \"L\",\"m\", \"M\",\"n\", \"N\",\"o\", \"O\",\"p\", \"P\",\"q\", \"Q\",\"r\", \"R\",\"s\", \"S\",\"t\", \"T\",\"u\", \"U\",\"v\", \"V\",\"w\", \"W\",\"x\", \"X\",\"y\", \"Y\",\"z\", \"Z\"";
	}

	@Override public String visitNumbers(@NotNull ALParser.NumbersContext ctx) {

		return "\"0\",\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\"";
	}

	@Override public String visitAutomatonExpression(@NotNull ALParser.AutomatonExpressionContext ctx) {

		if (ctx.third != null) {
			return ctx.first.getText() + "." + ctx.second.getText() + "." + 
					"getStateByName(\"" + ctx.third.getText() + "\")";
		}
		else {
			return ctx.getText();
		}
	}

	/*
		automaton("hello world");
		=> 
	 	Automaton.makeAutomatonFor("newName", "hello world");
	*/
	@Override public String visitAutomaton(@NotNull ALParser.AutomatonContext ctx)
	{
		// ParserRuleContext parent = ctx.getParent();
		// while (!(parent instanceof ALParser.InitDeclaratorContext)) {
		// 	System.out.println("sup");
		// 	parent = parent.getParent();
		// }
		// String id = ((ALParser.InitDeclaratorContext)parent).declarator().ID().getText();
		String id = extractID(ctx);
		return "Automaton.makeAutomatonFor(\"" + id + "\", " + ctx.STRING() + ")";
	}

	@Override public String visitFunctionExpression(@NotNull ALParser.FunctionExpressionContext ctx) 
	{ 
		String res = "";
		if (ctx.autoPrint() != null) {
			res += ctx.autoPrint().ID().getText() + ".printAutomaton()";
		} else if (ctx.print() != null) {
			res += "System.out.println(" + ctx.print().children.get(2).getText() +")";
		} else if (ctx.printResult() != null) {
			res += ctx.printResult().parameter().getText() + ".printResult(" + ctx.printResult().STRING() +")";
		}

		return res;
	}


	// for loops
	@Override public String visitIterationStatement(@NotNull ALParser.IterationStatementContext ctx) 
	{ 
		String res = tabs() + "for (";
		if (ctx.NUM() != null) {
			res += "int i = 0; i < " + ctx.NUM() + "; i++)\n";
		} else {
			if (ctx.DECLCHAR() != null) {
				res += "String ";
			} else {
				res += visit(ctx.typeSpecifier()) + " ";
			}
			res += ctx.ID().getText() + " : ";
			if (ctx.twoLevel() != null) {
				res += ctx.twoLevel().getText();
			}
			else {
				res += visit(ctx.functionExpressionWithReturn());
			}
			res += ")\n";


		}
		res += visit(ctx.compoundStatement());
		return res;
	}

	@Override public String visitCompoundStatement(@NotNull ALParser.CompoundStatementContext ctx) { 
		String res = tabs() + "{\n";
		curTab++;
		res += visit(ctx.statementList());
		curTab--;
		res += tabs() + "}\n";
		return res;
	}

	@Override public String visitEclosure(@NotNull ALParser.EclosureContext ctx) {
		String res = ctx.ID().getText() + ".epsilonClosure(" + visit(ctx.threeLevel()) + ")" ;
		return res;
	}

	@Override public String visitThreeLevel(@NotNull ALParser.ThreeLevelContext ctx) {
		if (ctx.ID().size() == 3) {
			return ctx.ID().get(0).getText() + "." + ctx.ID().get(1).getText() 
				+ ".getStateByName(\"" + ctx.ID().get(2).getText() + "\"" + ")";
		} else {
			return ctx.getText();
		}
		
	}

	@Override public String visitSelectionStatement(@NotNull ALParser.SelectionStatementContext ctx) {

		String res = tabs();
		res += "if (";
		if (ctx.ifnot != null) {
			res += "!";
		}
		res += visit(ctx.ifexp) + ")\n";
		res += visit(ctx.ifstat);
		if (ctx.elifexp != null) {
			res += tabs() + "else if (";
			if (ctx.elifnot != null) {
				res += "!";
			}
			res += visit(ctx.elifexp) + ")\n";
			res += visit(ctx.elifstat);
		}
		if (ctx.ELSE() != null) {
			res += tabs() + "else \n";
			res += visit(ctx.elsestat);
		}

		return res;
	}

	@Override public String visitMakeDFAFromStateSets(@NotNull ALParser.MakeDFAFromStateSetsContext ctx) {
		String res = "";
		res += "Automaton." + ctx.MAKEDFAFROMSTATESETS().getText();
		String id = extractID(ctx);
		res += "(\"" + id + "\", " + ctx.parameter().get(0).getText() + ", " + ctx.parameter().get(1).getText() + ")";
		return res;
	}




	@Override public String visitAssignmentExpression(@NotNull ALParser.AssignmentExpressionContext ctx) {
		String res = "";
		if (ctx.trans() != null) {
			// need to get charlist
			if (ctx.trans().charList() != null) {
				String charListString = ctx.trans().charList().getText();
				charListString = charListString.replaceAll("\'", "\"");
				String[] charListSplit = charListString.split(",|\\s");
				for (String sym : charListSplit) {
					res += ctx.trans().threeLevel().getText() + ".addTransitionsTo(" + visit(ctx.assignmentExpression()) + ", " + sym + ")";
				}
			} else {
				// ID()
				res += ctx.trans().threeLevel().getText() + ".addTransitionsTo(" + visit(ctx.assignmentExpression()) + ", " + ctx.trans().ID().getText() + ")";
			}
				
		} else if (ctx.ID() != null) {
			res += ctx.ID().getText() + " = ";
			res += visit(ctx.assignmentExpression());
		} else if (ctx.logicalFirstExpression() != null) {
			res += visit(ctx.logicalFirstExpression());
		}
		return res;
	}

}