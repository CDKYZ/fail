/**
 * Representation of Scope in Semantic checking.
 * 
 * Part of AL (Automaton Language)
 * 
 * @author Christopher Erlendson
 * Programming Languages and Translators
 *
 * Prof. Al Aho
 * TA: Will Falk-Wallace
 * 
 * Spring 2014
 */

package al.parser;
import al.parser.*;
public interface Scope {
    public String getScopeName();

    /** Where to look next for symbols */
    public Scope getEnclosingScope();

    /** Define a symbol in the current scope */
    public void define(Symbol sym);

    /** Look up name in this scope or in enclosing scope if not here */
    public Symbol resolve(String name);
}
