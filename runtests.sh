#!/bin/bash

#
# Script that compiles and runs source code. Without a parameter, runs all files in tests/.
# 
# Part of AL (Automaton Language)
# 
# @author Yujin Ariza
# @author Kristie Howard
# Programming Languages and Translators

# Prof. Al Aho
# TA: Will Falk-Wallace
# 
# Spring 2014
#

globallog=alltests.log
rm -f $globallog
error=0
passes=0
tries=0

RunTests() {
	basename=$(echo $1 | sed 's/.*\///' | sed 's/.al//')
	reffile=$(echo $1 | sed 's/.al$/.ref/')
	javafile=$(echo $1 | sed 's/.al$/.java/')
	directory=$(echo $1 | sed 's/\/[^/]+\.al/\//')
	outfile=$(echo $1 | sed 's/.al$/.out/')
	difffile=$(echo $1 | sed 's/.al$/.diff/')

	echo "testing $javafile..."

	./al.sh $1 >> $globallog &&
	javac $javafile -classpath "runtimelib/boilerplate.jar" -d "runtimebuild" 2>> $globallog &&
	java -cp "runtimebuild:runtimelib/boilerplate.jar" $basename | tee $outfile &&
	diff -B $outfile $reffile | tee $difffile

	cat $outfile >> $globallog

	if [ $? -ne 0 ]
		then
		echo $1 FAILED
		error=1
	elif [ -s $difffile ]
		then
		echo $1 FAILED
		error=1
	else
		echo $1 PASSED
		((passes++))
	fi
	((tries++))
	
	printf "\n"
}

mkdir runtimebuild

if [ $# -eq 0 ]
	then
	for file in tests/test*.al
	do 
		RunTests $file 2>> $globallog
	done
	else
	RunTests $1
fi

echo $passes / $tries passed.

exit $error
